#pragma once

#include "glm.hpp"
#include "Buffers.h"
#include "../../assets/shaders/registers.hlsl"
#include "../source/TextureManager.h"
#include <vector>

namespace render
{
	class LightSystem
	{
		public:
			struct Sunlight
			{
				glm::vec3 color;
				float power;
				glm::vec3 direction;
				float diffusePower;
				float specularPower;
				float pad[3];
			};
			struct Pointlight
			{
				glm::vec3 color;
				float power;
				glm::vec3 position;
				float diffusePower;
				float specularPower;
				float pad[3];
			};
			struct Spotlight
			{
				glm::vec3 color;
				float power;
				glm::vec3 position;
				float diffusePower;
				glm::vec3 direction;
				float specularPower;
				glm::vec3 upDirection;
				float innerCutoff;
				glm::vec3 rightDirection;
				float outerCutoff;
			};

			LightSystem(LightSystem& other) = delete;
			void operator=(const LightSystem&) = delete;
			static LightSystem* getInstance();

			void init();
			void deinit();
			void updateAndBind(glm::vec3 cameraPosition);
			void addSunlight(const Sunlight& light, int transformID);
			void addPointlight(const Pointlight& light, int transformID);
			void addSpotlight(const Spotlight& light, int transformID);
			void setAmbientColor(const glm::vec3& color) { ambientColor = color; }
			void setSpotlightMask(const engine::Texture& mask) { spotlightMask = mask; }

		protected:
			struct Lights
			{
				Sunlight sunLights[MAX_SUNLIGHTS];
				Pointlight pointLights[MAX_POINTLIGHTS];
				Spotlight spotLights[MAX_SPOTLIGHTS];
				uint32_t numSunlights;
				glm::vec3 ambientColor;
				uint32_t numPointlights;
				uint32_t numSpotlights;
				float pad[2];
			};
			engine::ConstantBuffer<Lights> lightData;

			std::vector<std::pair<Sunlight, int>> sunlights;
			std::vector<std::pair<Pointlight, int>> pointlights;
			std::vector<std::pair<Spotlight, int>> spotlights;
			engine::Texture spotlightMask;

			glm::vec3 ambientColor;
		private:
			LightSystem() {};
			~LightSystem() {};

			inline static LightSystem* instance{ nullptr };
			inline static std::mutex mutex{};
	};
}