#pragma once

#include "DxRes.h"
#include "D3D.h"
#include "../include/assert.hpp"
#include <vector>

namespace engine
{
	class BufferInterface
	{
	public:
		~BufferInterface() { deinit(); };
		void map(D3D11_MAPPED_SUBRESOURCE& ms) const
		{
			d3d::s_devcon->Map(buffer.ptr(), NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);
		}
		void unmap() const
		{
			d3d::s_devcon->Unmap(buffer.ptr(), NULL);
		}
		void deinit()
		{
			buffer.release();
		}
	protected:
		DxResPtr<ID3D11Buffer> buffer;
	};

	class IndexBuffer : public BufferInterface
	{
	public:
		void init(const std::vector<unsigned>& indices, D3D11_USAGE usage)
		{
			buffer.release();
			this->instances = indices.size();
			D3D11_BUFFER_DESC indexBufferDesc = {};
			indexBufferDesc.Usage = usage;
			indexBufferDesc.ByteWidth = sizeof(unsigned) * instances;
			indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

			D3D11_SUBRESOURCE_DATA indexData = {};
			indexData.pSysMem = indices.data();

			HRESULT hr = engine::d3d::s_device->CreateBuffer(&indexBufferDesc, &indexData, buffer.reset());
			ALWAYS_ASSERT(SUCCEEDED(hr));		
		}
		void bind(/*...*/) const
		{
			d3d::s_devcon->IASetIndexBuffer(buffer, DXGI_FORMAT_R32_UINT, 0);
		}
		int size() const { return instances; }
	protected:
		int instances = 0;
	};

	template <class T> class VertexBuffer : public BufferInterface
	{
	public:
		VertexBuffer() {};
		void init(const std::vector<T>& vertices, D3D11_USAGE usage)
		{
			buffer.release();
			instances = vertices.size();
			D3D11_BUFFER_DESC vertexBufferDesc = {};
			vertexBufferDesc.Usage = usage;
			vertexBufferDesc.ByteWidth = sizeof(T) * instances;
			vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

			D3D11_SUBRESOURCE_DATA vertexData = {};
			vertexData.pSysMem = vertices.data();

			HRESULT hr = engine::d3d::s_device->CreateBuffer(&vertexBufferDesc, &vertexData, buffer.reset());
			ALWAYS_ASSERT(SUCCEEDED(hr));
		}

		void init(int totalInstances, D3D11_USAGE usage)
		{
			if (totalInstances <= instances)
				return;
			buffer.release();
			instances = totalInstances;
			D3D11_BUFFER_DESC vertexBufferDesc = {};
			vertexBufferDesc.Usage = usage;
			vertexBufferDesc.ByteWidth = sizeof(T) * instances;
			vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

			HRESULT hr = engine::d3d::s_device->CreateBuffer(&vertexBufferDesc, NULL, buffer.reset());
			ALWAYS_ASSERT(SUCCEEDED(hr));
		}

		void bind(int slot) const
		{
			unsigned stride = sizeof(T);
			unsigned offset = 0;
			auto pVbuffer = buffer.ptr();
			d3d::s_devcon->IASetVertexBuffers(slot, 1, &pVbuffer, &stride, &offset);
		}
		int size() const { return instances; }
	protected:
		int instances = 0;
	};

	template <class T> class ConstantBuffer : public BufferInterface
	{
	public:
		void init(D3D11_USAGE usage)
		{
			D3D11_BUFFER_DESC constantBufferDesc = {};
			constantBufferDesc.Usage = usage;
			constantBufferDesc.ByteWidth = sizeof(T);
			constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			constantBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

			HRESULT hr = engine::d3d::s_device->CreateBuffer(&constantBufferDesc, NULL, buffer.reset());
			ALWAYS_ASSERT(SUCCEEDED(hr));
		}
		void bind(int slot, int shader) const
		{
			auto pCbuffer = buffer.ptr();
			if (shader == 0)
				d3d::s_devcon->PSSetConstantBuffers(slot, 1, &pCbuffer);
			else if (shader == 1)
				d3d::s_devcon->VSSetConstantBuffers(slot, 1, &pCbuffer);
		}
		void update(T& data) const
		{	
			D3D11_MAPPED_SUBRESOURCE ms;
			map(ms);
			memcpy(ms.pData, &data, sizeof(T));
			unmap();
		}
	};
}