#pragma once

#include <string>
#include <unordered_map>
#include "DxRes.h"
#include "D3D.h"

namespace render
{
	class ShaderManager
	{
	public:
		ShaderManager(ShaderManager& other) = delete;
		void operator=(const ShaderManager&) = delete;
		static ShaderManager* getInstance();

		void addVertexShader(std::string_view filename, bool inputLayout = true);
		void addInputLayout(std::string_view filename, ID3DBlob* vsBlob);
		void addPixelShader(std::string_view filename);
		void bindVS(std::string_view filename);
		void bindPS(std::string_view filename);
		void deinit();
		void init();
	protected:
		std::unordered_map<std::string, engine::DxResPtr<ID3D11VertexShader>> vertexShaders;
		std::unordered_map<std::string, engine::DxResPtr<ID3D11PixelShader>> pixelShaders;
		std::unordered_map<std::string, engine::DxResPtr<ID3D11InputLayout>> inputLayouts;
	private:
		ShaderManager() {};
		~ShaderManager() {};

		inline static ShaderManager* instance{ nullptr };
		inline static std::mutex mutex{};
	};
}
