#pragma once

#include "DxRes.h"
#include "D3D.h"
#include "Buffers.h"
#include "glm.hpp"
#include "../source/TextureManager.h"

namespace engine
{
	struct PerFrameConstantBuffer;
}

namespace render
{
	class PostProcess
	{
	public:
		static void resolve(engine::DxResPtr<ID3D11ShaderResourceView> src, engine::DxResPtr<ID3D11RenderTargetView> dst);
	};

	class Renderer
	{
	public:
		enum ShaderType {PS = 0, VS = 1};
		enum BufferType {PER_FRAME, PER_VIEW};

		struct PerViewConstantBuffer
		{
			glm::vec4 viewProj0;
			glm::vec4 viewProj1;
			glm::vec4 viewProj2;
			glm::vec4 viewProj3;
			glm::vec4 frustumOrigin;
			glm::vec4 frustumU;
			glm::vec4 frustumV;
			glm::vec3 cameraPosition;
			float padding;
		};
		Renderer(Renderer& other) = delete;
		void operator=(const Renderer&) = delete;
		static Renderer* getInstance();

		void init();
		void deinit();
		void initDepthAndRTV(engine::DxResPtr<ID3D11Texture2D> renderTarget, int width, int height);
		void releaseRTV();
		void render() const;
		void updateViewBuffer(glm::mat4 viewProj, glm::vec4 frustumOrigin, glm::vec4 frustumU, glm::vec4 frustumV, glm::vec3 cameraPosition);
		void bindBuffer(int slot, int shader, int type) const;
		void setSkyTexture(const engine::Texture& texture);
	protected:
		void initDepthBuffer(engine::DxResPtr<ID3D11Texture2D> renderTarget, int width, int height);
		void initSamplers();
		void bindSamplers() const;
		void renderSkybox() const;
		engine::DxResPtr<ID3D11RenderTargetView> renderTargetView;
		engine::DxResPtr<ID3D11RenderTargetView> renderTargetViewHDR;
		engine::DxResPtr<ID3D11ShaderResourceView> resourceViewHDR;
		engine::DxResPtr<ID3D11Texture2D> renderTargetHDR;
		engine::DxResPtr<ID3D11Texture2D> depthStencil;
		engine::DxResPtr<ID3D11DepthStencilState> depthStencilState;
		engine::DxResPtr<ID3D11DepthStencilState> depthStencilReadOnlyState;
		engine::DxResPtr<ID3D11DepthStencilView> depthStencilView;
		engine::ConstantBuffer<engine::PerFrameConstantBuffer> perFrameBuffer;
		engine::ConstantBuffer<PerViewConstantBuffer> perViewBuffer;
		engine::DxResPtr<ID3D11SamplerState> pointWrapSampler;
		engine::DxResPtr<ID3D11SamplerState> linearWrapSampler;
		engine::DxResPtr<ID3D11SamplerState> anisotropicWrapSampler;
		engine::Texture skyTexture;
		glm::vec3 cameraPosition;
	private:
		Renderer() {};
		~Renderer() {};

		inline static Renderer* instance{ nullptr };
		inline static std::mutex mutex{};
	};
}
