#pragma once

#include <vector>
#include <memory>
#include "../source/ModelManager.h"
#include "../source/TextureManager.h"
#include "../render/Buffers.h"
#include "../source/Mesh.h"
#include "../source/math/Ray.h"
#include "../source/ObjectDecorators.h"
#include "D3D.h"
#include <memory>

namespace render
{
	enum class IntersectedType { HologramInstance, NormalVisInstance, TextureOnlyInstance, OpaqueInstance, EmissiveInstance, NUM };
	struct ObjRef
	{
		void* object;
		IntersectedType type;
		int instanceID;
		int transformID;
	};
	struct IntersectionQuery
	{
		math::Intersection nearest;
		std::unique_ptr<IObjectMover> mover;
	};

	template<typename Mat, typename Inst> class ShaderGroupInstances
	{
	public:
		void updateInstanceBuffers(glm::vec3 cameraPosition);
		void render() const;
		void add(std::shared_ptr<engine::Model> model,
			const std::vector<Mat>& materials, const Inst& instance, int instanceID, int transformID);
		void init();
		void deinit();
		bool intersect(const math::Ray& ray, ObjRef& outRef, math::Intersection& outNearest);
	protected:
		struct MeshData
		{
			glm::vec4 meshToModel0;
			glm::vec4 meshToModel1;
			glm::vec4 meshToModel2;
			glm::vec4 meshToModel3;
		};

		struct MaterialData
		{
			float padding[4];
		};

		struct InstanceByID
		{
			int instanceID;
			int transformID;
			Inst instance;
		};

		struct PerMaterial
		{
			Mat material;
			std::vector<InstanceByID> instances;
		};

		struct PerMeshInstance
		{
			int meshInstanceIdx;
			std::vector<PerMaterial> perMaterials;
		};

		struct PerMesh
		{
			std::vector<PerMeshInstance> perMeshInstances;
		};

		struct PerModel
		{
			std::shared_ptr<engine::Model> model;
			std::vector<PerMesh> perMesh;
		};

		IntersectedType getIntersectionType();
		void bindMaterial(const Mat& material) const;

		std::vector<PerModel> perModels;
		engine::VertexBuffer<Inst> instanceBuffer;
		engine::ConstantBuffer<MeshData> meshData;
		engine::ConstantBuffer<MaterialData> materialData;
		static const char* psName;
		static const char* vsName;
		int currentInstanceID = 0;
	};

	struct HologramInstance
	{
		glm::mat4 modelToWorld;
		glm::vec4 color;
	};

	struct TextureOnlyInstance
	{
		glm::mat4 modelToWorld;
	};

	struct NormalVisInstance
	{
		glm::mat4 modelToWorld;
	};

	struct OpaqueInstance
	{
		glm::mat4 modelToWorld;
	};

	struct EmissiveInstance
	{
		glm::mat4 modelToWorld;
		glm::vec4 color;
	};

	struct Material
	{
		bool compare(const Material& other) const
		{
			return true;
		}
	};

	struct TextureMaterial
	{
		engine::Texture texture;
		bool compare(const TextureMaterial& other) const
		{
			return other.texture.textureView.ptr() == texture.textureView.ptr();
		}
	};

	class MeshSystem
	{
	public:
		MeshSystem(MeshSystem& other) = delete;
		void operator=(const MeshSystem&) = delete;
		static MeshSystem* getInstance();

		void init();
		void deinit();
		void render(glm::vec3 cameraPosition);
		bool findIntersection(const math::Ray& ray, IntersectionQuery& query);

		void addTextureOnlyInstance(std::shared_ptr<engine::Model> model, const std::vector<TextureMaterial>& textureMaterials, int transformID);
		void addOpaqueInstance(std::shared_ptr<engine::Model> model, const std::vector<TextureMaterial>& textureMaterials, int transformID);
		void addHologramInstance(std::shared_ptr<engine::Model> model, const glm::vec3& color, int transformID);
		void addEmissiveInstance(std::shared_ptr<engine::Model> model, const glm::vec3& color, int transformID);
		void addNormalVisInstance(std::shared_ptr<engine::Model> model, int transformID);

	protected:
		void findIntersectionInternal(const math::Ray& ray, ObjRef& outRef, math::Intersection& outNearest);

		ShaderGroupInstances<Material, HologramInstance> hologramInstances;
		ShaderGroupInstances<Material, NormalVisInstance> normalVisInstances;
		ShaderGroupInstances<TextureMaterial, TextureOnlyInstance> textureOnlyInstances;
		ShaderGroupInstances<TextureMaterial, OpaqueInstance> opaqueInstances;
		ShaderGroupInstances<Material, EmissiveInstance> emissiveInstances;
		int currentInstanceID = 0;
	private:
		MeshSystem() {};
		~MeshSystem() {};

		inline static MeshSystem* instance{ nullptr };
		inline static std::mutex mutex{};
	};
}
