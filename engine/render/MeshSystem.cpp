#include "MeshSystem.h"

#include "ShaderManager.h"
#include "Renderer.h"
#include "../../assets/shaders/registers.hlsl"
#include "../source/TransformSystem.h"

namespace render
{

	const char* ShaderGroupInstances<Material, HologramInstance>::psName = "..\\assets\\shaders\\hologram.hlsl";
	const char* ShaderGroupInstances<Material, HologramInstance>::vsName = "..\\assets\\shaders\\hologram.hlsl";

	const char* ShaderGroupInstances<TextureMaterial, TextureOnlyInstance>::psName = "..\\assets\\shaders\\texture.hlsl";
	const char* ShaderGroupInstances<TextureMaterial, TextureOnlyInstance>::vsName = "..\\assets\\shaders\\texture.hlsl";

	const char* ShaderGroupInstances<Material, NormalVisInstance>::psName = "..\\assets\\shaders\\normalVisual.hlsl";
	const char* ShaderGroupInstances<Material, NormalVisInstance>::vsName = "..\\assets\\shaders\\normalVisual.hlsl";

	const char* ShaderGroupInstances<TextureMaterial, OpaqueInstance>::psName = "..\\assets\\shaders\\blinn_phong.hlsl";
	const char* ShaderGroupInstances<TextureMaterial, OpaqueInstance>::vsName = "..\\assets\\shaders\\blinn_phong.hlsl";

	const char* ShaderGroupInstances<Material, EmissiveInstance>::psName = "..\\assets\\shaders\\emissive.hlsl";
	const char* ShaderGroupInstances<Material, EmissiveInstance>::vsName = "..\\assets\\shaders\\emissive.hlsl";

	template<typename Mat, typename Inst> void ShaderGroupInstances<Mat, Inst>::updateInstanceBuffers(glm::vec3 cameraPosition)
	{
		uint32_t totalInstances = 0;
		for (auto& perModel : perModels)
			for (auto& perMesh : perModel.perMesh)
				for (const auto& perMeshInstance : perMesh.perMeshInstances)
					for (const auto& perMaterial : perMeshInstance.perMaterials)
						totalInstances += uint32_t(perMaterial.instances.size());

		if (totalInstances == 0)
			return;

		instanceBuffer.init(totalInstances, D3D11_USAGE_DYNAMIC); // resizes if needed

		D3D11_MAPPED_SUBRESOURCE ms;
		instanceBuffer.map(ms);
		Inst* dst = static_cast<Inst*>(ms.pData);

		engine::TransformSystem* transformSystem = engine::TransformSystem::getInstance();

		uint32_t copiedNum = 0;
		for (auto& perModel : perModels)
		{
			for (uint32_t meshIndex = 0; meshIndex < perModel.perMesh.size(); ++meshIndex)
			{
				engine::Mesh& mesh = perModel.model->meshes[meshIndex];

				for (auto& perMeshInstance : perModel.perMesh[meshIndex].perMeshInstances)
					for (auto& perMaterial : perMeshInstance.perMaterials)
					{
						auto& instances = perMaterial.instances;

						uint32_t numModelInstances = instances.size();
						for (uint32_t index = 0; index < numModelInstances; ++index)
						{
							instances[index].instance.modelToWorld = transformSystem->transformMat(instances[index].transformID);
							instances[index].instance.modelToWorld[3] -= glm::vec4(cameraPosition, 0.f);
							dst[copiedNum++] = instances[index].instance;
						}
					}
			}
		}

		instanceBuffer.unmap();
	}
	template<typename Mat, typename Inst> void ShaderGroupInstances<Mat, Inst>::add(std::shared_ptr<engine::Model> model,
		const std::vector<Mat>& materials, const Inst& groupInstance, int currentInstanceID, int transformID)
	{
		InstanceByID instance = { currentInstanceID, transformID, groupInstance };
		int meshCount = model->meshes.size();
		auto matchingName = [model](PerModel p) { return p.model->name == model->name; };
		auto perModel = std::find_if(perModels.begin(), perModels.end(), matchingName);
		if (perModel != perModels.end())
		{
			for (int i = 0; i < perModel->perMesh.size(); i++)
			{
				auto& perMesh = perModel->perMesh[i];
				for (auto& perMeshInstance : perMesh.perMeshInstances)
				{
					bool materialExists = false;
					for (int j = 0; j < perMeshInstance.perMaterials.size(); j++)
					{
						if (materials.size() <= i || perMeshInstance.perMaterials[j].material.compare(materials[i]))
						{
							materialExists = true;
							perMeshInstance.perMaterials[j].instances.push_back(instance);
							break;
						}
					}
					if (!materialExists)
					{
						PerMaterial perMaterial;
						perMaterial.material = materials[i];
						perMaterial.instances.push_back(instance);
						perMeshInstance.perMaterials.push_back(perMaterial);
					}		
				}
			}
		}
		else
		{
			PerModel perModel;
			perModel.model = model;
			perModels.emplace_back(perModel);
			for (int i = 0; i < meshCount; i++)
			{
				PerMesh perMesh;
				for (int j = 0; j < model->meshes[i].instances.size(); j++)
				{
					PerMaterial perMaterial;
					if (materials.size() > i)
						perMaterial.material = materials[i];
					perMaterial.instances.push_back(instance);
					PerMeshInstance perMeshInstance;
					perMeshInstance.perMaterials.push_back(perMaterial);
					perMeshInstance.meshInstanceIdx = j;
					perMesh.perMeshInstances.push_back(perMeshInstance);
				}

				perModels.back().perMesh.push_back(perMesh);
			}
		}
	}

	template<typename Mat, typename Inst> void ShaderGroupInstances<Mat, Inst>::init()
	{
		meshData.init(D3D11_USAGE_DYNAMIC);
		materialData.init(D3D11_USAGE_DYNAMIC);
	}

	template<typename Mat, typename Inst> void ShaderGroupInstances<Mat, Inst>::deinit()
	{
		perModels.clear();
		instanceBuffer.deinit();
		meshData.deinit();
		materialData.deinit();
	}

	template<typename Mat, typename Inst> bool ShaderGroupInstances<Mat, Inst>::intersect(const math::Ray& ray, ObjRef& outRef, math::Intersection& outNearest)
	{
		bool found = false;
		for (int modelIdx = 0; modelIdx < perModels.size(); modelIdx++)
			for (int meshIdx = 0; meshIdx < perModels[modelIdx].perMesh.size(); meshIdx++)
				for (auto& perMeshInstance : perModels[modelIdx].perMesh[meshIdx].perMeshInstances)
					for (auto& perMaterial : perMeshInstance.perMaterials)
						for (int instanceIdx = 0; instanceIdx < perMaterial.instances.size(); instanceIdx++)
						{
							auto& instance = perMaterial.instances[instanceIdx].instance;
							auto& meshToModel = perModels[modelIdx].model->meshes[meshIdx].instances[perMeshInstance.meshInstanceIdx];
							glm::mat4 modelMat = glm::transpose(instance.modelToWorld * meshToModel);
							glm::mat4 modelInv;
							math::invertOrthogonal(modelMat, modelInv);

							engine::MeshIntersection mNearest;
							math::Ray modelRay;
							modelRay.set(glm::vec4(ray.origin, 1.f) * modelInv, glm::vec4(ray.direction, 0.f) * modelInv);
							mNearest.reset();

							found = perModels[modelIdx].model->meshes[meshIdx].intersect(modelRay, mNearest);
							if (found)
							{
								float t = glm::length(glm::vec3(glm::vec4(mNearest.t * modelRay.direction + modelRay.origin, 1.f) * modelMat) - ray.origin);
								if (t < outNearest.t)
								{
									outRef.type = getIntersectionType();
									outRef.object = static_cast<void*>(&instance);
									outRef.instanceID = perMaterial.instances[instanceIdx].instanceID;
									outRef.transformID = perMaterial.instances[instanceIdx].transformID;
									outNearest.normal = glm::normalize(glm::vec3(glm::vec4(mNearest.normal, 0.f) * modelMat));
									outNearest.pos = glm::vec4(mNearest.pos, 1.f) * modelMat;
									outNearest.t = t;
								}
							}
						}
		return found;
	}

	IntersectedType ShaderGroupInstances<Material, HologramInstance>::getIntersectionType()
	{
		return IntersectedType::HologramInstance;
	}

	IntersectedType ShaderGroupInstances<Material, NormalVisInstance>::getIntersectionType()
	{
		return IntersectedType::NormalVisInstance;
	}

	IntersectedType ShaderGroupInstances<TextureMaterial, TextureOnlyInstance>::getIntersectionType()
	{
		return IntersectedType::TextureOnlyInstance;
	}

	IntersectedType ShaderGroupInstances<TextureMaterial, OpaqueInstance>::getIntersectionType()
	{
		return IntersectedType::OpaqueInstance;
	}

	IntersectedType ShaderGroupInstances<Material, EmissiveInstance>::getIntersectionType()
	{
		return IntersectedType::EmissiveInstance;
	}

	void ShaderGroupInstances<Material, HologramInstance>::bindMaterial(const Material& material) const
	{}

	void ShaderGroupInstances<Material, EmissiveInstance>::bindMaterial(const Material& material) const
	{}

	void ShaderGroupInstances<TextureMaterial, TextureOnlyInstance>::bindMaterial(const TextureMaterial& material) const
	{
		// ... update shader local per-draw uniform buffer
		MaterialData data;
		materialData.update(data); // we don't have it in HW4

		ID3D11ShaderResourceView* textureView = material.texture.textureView.ptr();
		engine::d3d::s_devcon->PSSetShaderResources(0, 1, &textureView);
	}

	void ShaderGroupInstances<TextureMaterial, OpaqueInstance>::bindMaterial(const TextureMaterial& material) const
	{
		MaterialData data;
		materialData.update(data);

		ID3D11ShaderResourceView* textureView = material.texture.textureView.ptr();
		engine::d3d::s_devcon->PSSetShaderResources(0, 1, &textureView);
	}

	void ShaderGroupInstances<Material, NormalVisInstance>::bindMaterial(const Material& material) const
	{}

	template<typename Mat, typename Inst> void ShaderGroupInstances<Mat, Inst>::render() const
	{
		if (instanceBuffer.size() == 0)
			return;

		ShaderManager::getInstance()->bindVS(vsName);
		ShaderManager::getInstance()->bindPS(psName);

		instanceBuffer.bind(1);

		uint32_t renderedInstances = 0;
		for (const auto& perModel : perModels)
		{
			if (perModel.perMesh.empty()) continue;

			perModel.model->vertices.bind(0);
			perModel.model->indices.bind();

			for (uint32_t meshIndex = 0; meshIndex < perModel.perMesh.size(); ++meshIndex)
			{
				const engine::Mesh& mesh = perModel.model->meshes[meshIndex];
				const auto& meshRange = perModel.model->ranges[meshIndex];

				for (const auto& perMeshInstance : perModel.perMesh[meshIndex].perMeshInstances)
				{
					// You have to upload a Mesh-to-Model transformation matrix retrieved from model file via Assimp
					auto meshToModelT = glm::transpose(mesh.instances[perMeshInstance.meshInstanceIdx]);
					MeshData data = { meshToModelT[0], meshToModelT[1], meshToModelT[2], meshToModelT[3] };
					meshData.update(data); // ... update shader local per-mesh uniform buffer
					meshData.bind(MESH_TO_MODEL_CBUF, 1);

					for (const auto& perMaterial : perMeshInstance.perMaterials)
					{
						if (perMaterial.instances.empty()) continue;

						const auto& material = perMaterial.material;

						bindMaterial(material);

						uint32_t numInstances = uint32_t(perMaterial.instances.size());
						engine::d3d::s_devcon->DrawIndexedInstanced(meshRange.indexNum, numInstances, meshRange.indexOffset, meshRange.vertexOffset, renderedInstances);
						renderedInstances += numInstances;
					}
				}
			}
		}
	}

	MeshSystem* MeshSystem::getInstance()
	{
		std::lock_guard<std::mutex> lock(mutex);
		if (instance == nullptr)
		{
			instance = new MeshSystem();
		}
		return instance;
	}

	void MeshSystem::deinit()
	{
		hologramInstances.deinit();
		normalVisInstances.deinit();
		textureOnlyInstances.deinit();
		opaqueInstances.deinit();
		emissiveInstances.deinit();
	}

	void MeshSystem::init()
	{
		hologramInstances.init();
		normalVisInstances.init();
		textureOnlyInstances.init();
		opaqueInstances.init();
		emissiveInstances.init();
	}
	void MeshSystem::render(glm::vec3 cameraPosition)
	{
		hologramInstances.updateInstanceBuffers(cameraPosition);
		hologramInstances.render();
		normalVisInstances.updateInstanceBuffers(cameraPosition);
		normalVisInstances.render();
		textureOnlyInstances.updateInstanceBuffers(cameraPosition);
		textureOnlyInstances.render();
		opaqueInstances.updateInstanceBuffers(cameraPosition);
		opaqueInstances.render();
		emissiveInstances.updateInstanceBuffers(cameraPosition);
		emissiveInstances.render();
	}

	bool MeshSystem::findIntersection(const math::Ray& ray, IntersectionQuery& query)
	{
		ObjRef ref = { nullptr, IntersectedType::NUM, -1, -1 };

		findIntersectionInternal(ray, ref, query.nearest);
		if(ref.type < IntersectedType::NUM)
			query.mover.reset(new TransformMover(ref.transformID));
		
		return ref.type != IntersectedType::NUM;
	}
	void MeshSystem::findIntersectionInternal(const math::Ray& ray, ObjRef& outRef, math::Intersection& outNearest)
	{
		hologramInstances.intersect(ray, outRef, outNearest);
		normalVisInstances.intersect(ray, outRef, outNearest);
		textureOnlyInstances.intersect(ray, outRef, outNearest);
		opaqueInstances.intersect(ray, outRef, outNearest);
		emissiveInstances.intersect(ray, outRef, outNearest);
	}

	void MeshSystem::addTextureOnlyInstance(std::shared_ptr<engine::Model> model, const std::vector<TextureMaterial>& textureMaterials, int transformID)
	{
		TextureOnlyInstance instance;
		instance.modelToWorld = engine::TransformSystem::getInstance()->transformMat(transformID);
		textureOnlyInstances.add(model, textureMaterials, instance, currentInstanceID++, transformID);
	}

	void MeshSystem::addOpaqueInstance(std::shared_ptr<engine::Model> model, const std::vector<TextureMaterial>& textureMaterials, int transformID)
	{
		OpaqueInstance instance;
		instance.modelToWorld = engine::TransformSystem::getInstance()->transformMat(transformID);
		opaqueInstances.add(model, textureMaterials, instance, currentInstanceID++, transformID);
	}

	void MeshSystem::addHologramInstance(std::shared_ptr<engine::Model> model, const glm::vec3& color, int transformID)
	{
		HologramInstance instance;
		instance.modelToWorld = engine::TransformSystem::getInstance()->transformMat(transformID);
		instance.color = glm::vec4(color, 1.f);
		hologramInstances.add(model, std::vector<Material>(), instance, currentInstanceID++, transformID);
	}
	void MeshSystem::addEmissiveInstance(std::shared_ptr<engine::Model> model, const glm::vec3& color, int transformID)
	{
		EmissiveInstance instance;
		instance.modelToWorld = engine::TransformSystem::getInstance()->transformMat(transformID);
		instance.color = glm::vec4(color, 1.f);
		emissiveInstances.add(model, std::vector<Material>(), instance, currentInstanceID++, transformID);
	}
	void MeshSystem::addNormalVisInstance(std::shared_ptr<engine::Model> model, int transformID)
	{
		NormalVisInstance instance;
		instance.modelToWorld = engine::TransformSystem::getInstance()->transformMat(transformID);
		normalVisInstances.add(model, std::vector<Material>(), instance, currentInstanceID++, transformID);
	}
}