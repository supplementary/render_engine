#include "Renderer.h"

#include "assert.hpp"
#include "MeshSystem.h"
#include "LightSystem.h"
#include "../source/Engine.h"
#include "../../assets/shaders/registers.hlsl"

render::Renderer* render::Renderer::getInstance()
{
	std::lock_guard<std::mutex> lock(mutex);
	if (instance == nullptr)
	{
		instance = new Renderer();
	}
	return instance;
}

void render::Renderer::init()
{
	perFrameBuffer.init(D3D11_USAGE_DYNAMIC);
	perViewBuffer.init(D3D11_USAGE_DYNAMIC);
	initSamplers();
	ShaderManager::getInstance()->addVertexShader("..\\assets\\shaders\\skybox.hlsl", false);
}

void render::Renderer::initDepthBuffer(engine::DxResPtr<ID3D11Texture2D> renderTarget, int width, int height)
{
	D3D11_TEXTURE2D_DESC descDepth = {};
	descDepth.Width = width;
	descDepth.Height = height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDepth.SampleDesc.Count = 1;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;

	HRESULT hr = engine::d3d::s_device->CreateTexture2D(&descDepth, NULL, depthStencil.reset());
	ALWAYS_ASSERT(SUCCEEDED(hr));

	D3D11_DEPTH_STENCIL_DESC dsDesc = {};
	// Depth test parameters
	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_GREATER;
	// Stencil test parameters
	dsDesc.StencilEnable = false;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;
	// Stencil operations if pixel is front-facing
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil operations if pixel is back-facing
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	D3D11_DEPTH_STENCIL_DESC dsReadOnlyDesc = {};
	dsReadOnlyDesc.DepthEnable = true;
	dsReadOnlyDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	dsReadOnlyDesc.DepthFunc = D3D11_COMPARISON_EQUAL;
	dsReadOnlyDesc.StencilEnable = false;
	dsReadOnlyDesc.StencilReadMask = 0xFF;
	dsReadOnlyDesc.StencilWriteMask = 0xFF;
	dsReadOnlyDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsReadOnlyDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsReadOnlyDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsReadOnlyDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	dsReadOnlyDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsReadOnlyDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsReadOnlyDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsReadOnlyDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create depth stencil state
	hr = engine::d3d::s_device->CreateDepthStencilState(&dsDesc, depthStencilState.reset());
	ALWAYS_ASSERT(SUCCEEDED(hr));
	hr = engine::d3d::s_device->CreateDepthStencilState(&dsReadOnlyDesc, depthStencilReadOnlyState.reset());
	ALWAYS_ASSERT(SUCCEEDED(hr));

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV = {};
	descDSV.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;

	// Create the depth stencil view
	hr = engine::d3d::s_device->CreateDepthStencilView(depthStencil, &descDSV, depthStencilView.reset());
	ALWAYS_ASSERT(SUCCEEDED(hr));
}

void render::Renderer::initSamplers()
{
	D3D11_SAMPLER_DESC samplerDesc = {};
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	HRESULT hr = engine::d3d::s_device->CreateSamplerState(&samplerDesc, pointWrapSampler.reset());
	ALWAYS_ASSERT(SUCCEEDED(hr));

	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	hr = engine::d3d::s_device->CreateSamplerState(&samplerDesc, linearWrapSampler.reset());
	ALWAYS_ASSERT(SUCCEEDED(hr));

	samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 16;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	hr = engine::d3d::s_device->CreateSamplerState(&samplerDesc, anisotropicWrapSampler.reset());
	ALWAYS_ASSERT(SUCCEEDED(hr));
}

void render::Renderer::bindSamplers() const
{
	std::array<ID3D11SamplerState*, 3> samplerStates = { pointWrapSampler, linearWrapSampler, anisotropicWrapSampler };
	engine::d3d::s_devcon->PSSetSamplers(0, 3, samplerStates.data());
}

void render::Renderer::renderSkybox() const
{
	engine::d3d::s_devcon->OMSetDepthStencilState(depthStencilReadOnlyState, 1);
	ShaderManager::getInstance()->bindVS("..\\assets\\shaders\\skybox.hlsl");
	ShaderManager::getInstance()->bindPS("..\\assets\\shaders\\skybox.hlsl");

	ID3D11ShaderResourceView* textureView = skyTexture.textureView.ptr();
	engine::d3d::s_devcon->PSSetShaderResources(0, 1, &textureView);

	engine::d3d::s_devcon->Draw(3, 0);
}

void render::Renderer::deinit()
{
	renderTargetView.release();
	renderTargetViewHDR.release();
	resourceViewHDR.release();
	renderTargetHDR.release();
	depthStencil.release();
	depthStencilState.release();
	depthStencilReadOnlyState.release();
	depthStencilView.release();
	perFrameBuffer.deinit();
	perViewBuffer.deinit();
	pointWrapSampler.release();
	linearWrapSampler.release();
	anisotropicWrapSampler.release();
	skyTexture.textureView.release();
}

void render::Renderer::initDepthAndRTV(engine::DxResPtr<ID3D11Texture2D> renderTarget, int width, int height)
{
	D3D11_TEXTURE2D_DESC descDepth = {};
	descDepth.Width = width;
	descDepth.Height = height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	descDepth.SampleDesc.Count = 1;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;;

	HRESULT hr = engine::d3d::s_device->CreateTexture2D(&descDepth, NULL, renderTargetHDR.reset());
	ALWAYS_ASSERT(SUCCEEDED(hr));

	hr = engine::d3d::s_device->CreateRenderTargetView(renderTargetHDR, NULL, renderTargetViewHDR.reset());
	ALWAYS_ASSERT(SUCCEEDED(hr));

	hr = engine::d3d::s_device->CreateShaderResourceView(renderTargetHDR, NULL, resourceViewHDR.reset());
	ALWAYS_ASSERT(SUCCEEDED(hr));

	hr = engine::d3d::s_device->CreateRenderTargetView(renderTarget, NULL, renderTargetView.reset());
	ALWAYS_ASSERT(SUCCEEDED(hr));

	initDepthBuffer(renderTargetHDR, width, height);
}

void render::Renderer::releaseRTV()
{
	renderTargetView.release();
	renderTargetViewHDR.release();
	renderTargetHDR.release();
	resourceViewHDR.release();
}

void render::Renderer::render() const
{
	bindSamplers();
	// Bind the depth stencil view
	auto pRTVs = renderTargetViewHDR.ptr();
	engine::d3d::s_devcon->OMSetRenderTargets(1, &pRTVs, depthStencilView);

	float backgroundColor[4] = { 0x64 / 255.0f, 0x95 / 255.0f, 0xED / 255.0f, 1.0f };
	engine::d3d::s_devcon->ClearRenderTargetView(renderTargetViewHDR, backgroundColor);
	engine::d3d::s_devcon->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 0.f, 0);
	engine::d3d::s_devcon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	perFrameBuffer.update(engine::Engine::g_frameCBuffer);
	bindBuffer(PER_FRAME_CBUF, Renderer::ShaderType::PS, Renderer::BufferType::PER_FRAME);
	bindBuffer(PER_VIEW_CBUF, Renderer::ShaderType::PS, Renderer::BufferType::PER_VIEW);
	bindBuffer(PER_FRAME_CBUF, Renderer::ShaderType::VS, Renderer::BufferType::PER_FRAME);
	bindBuffer(PER_VIEW_CBUF, Renderer::ShaderType::VS, Renderer::BufferType::PER_VIEW);
	LightSystem::getInstance()->updateAndBind(cameraPosition);

	engine::d3d::s_devcon->OMSetDepthStencilState(depthStencilState, 1);
	MeshSystem::getInstance()->render(cameraPosition);
	renderSkybox();

	PostProcess::resolve(resourceViewHDR, renderTargetView);
}

void render::Renderer::updateViewBuffer(glm::mat4 viewProj, glm::vec4 frustumOrigin, glm::vec4 frustumU, glm::vec4 frustumV, glm::vec3 cameraPosition)
{
	auto viewProjT = viewProj;
	PerViewConstantBuffer vb = { viewProjT[0], viewProjT[1], viewProjT[2], viewProjT[3], frustumOrigin, frustumU, frustumV, cameraPosition };
	this->cameraPosition = cameraPosition;
	perViewBuffer.update(vb);
}

void render::Renderer::bindBuffer(int slot, int shader, int type) const
{
	if (type == BufferType::PER_FRAME)
		perFrameBuffer.bind(slot, shader);
	else if (type == BufferType::PER_VIEW)
		perViewBuffer.bind(slot, shader);
}

void render::Renderer::setSkyTexture(const engine::Texture& texture)
{
	skyTexture = texture;
}

void render::PostProcess::resolve(engine::DxResPtr<ID3D11ShaderResourceView> src, engine::DxResPtr<ID3D11RenderTargetView> dst)
{
	auto pRTVs = dst.ptr();
	engine::d3d::s_devcon->OMSetRenderTargets(1, &pRTVs, NULL);

	float backgroundColor[4] = { 0x64 / 255.0f, 0x95 / 255.0f, 0xED / 255.0f, 1.0f };
	engine::d3d::s_devcon->ClearRenderTargetView(dst, backgroundColor);
	engine::d3d::s_devcon->OMSetDepthStencilState(NULL, 0);

	ShaderManager::getInstance()->bindVS("..\\assets\\shaders\\resolve.hlsl");
	ShaderManager::getInstance()->bindPS("..\\assets\\shaders\\resolve.hlsl");

	ID3D11ShaderResourceView* resourceView = src.ptr();
	engine::d3d::s_devcon->PSSetShaderResources(0, 1, &resourceView);

	engine::d3d::s_devcon->Draw(3, 0);

	ID3D11ShaderResourceView* const pSRV[1] = { NULL };
	engine::d3d::s_devcon->PSSetShaderResources(0, 1, pSRV);
}
