#include "LightSystem.h"

#include "../source/TransformSystem.h"
#include "Renderer.h"

namespace render
{
	LightSystem* LightSystem::getInstance()
	{
		std::lock_guard<std::mutex> lock(mutex);
		if (instance == nullptr)
		{
			instance = new LightSystem();
		}
		return instance;
	}
	void LightSystem::init()
	{
		lightData.init(D3D11_USAGE_DYNAMIC);
	}
	void LightSystem::deinit()
	{
		lightData.deinit();
		spotlightMask.textureView.release();
	}
	void LightSystem::updateAndBind(glm::vec3 cameraPosition)
	{
		engine::TransformSystem* transformSystem = engine::TransformSystem::getInstance();
		Lights lightBuf;
		lightBuf.numSunlights = std::min(MAX_SUNLIGHTS, (int)sunlights.size());
		for (int i = 0; i < lightBuf.numSunlights; i++)
		{
			sunlights[i].first.direction = glm::normalize(transformSystem->transformMat(sunlights[i].second)[2]);
			lightBuf.sunLights[i] = sunlights[i].first;
		}
		lightBuf.numPointlights = std::min(MAX_POINTLIGHTS, (int)pointlights.size());
		for (int i = 0; i < lightBuf.numPointlights; i++)
		{
			pointlights[i].first.position = transformSystem->transformMat(pointlights[i].second)[3] - glm::vec4(cameraPosition, 0.f);
			lightBuf.pointLights[i] = pointlights[i].first;
		}
		lightBuf.numSpotlights = std::min(MAX_SPOTLIGHTS, (int)spotlights.size());
		for (int i = 0; i < lightBuf.numSpotlights; i++)
		{
			auto mat = transformSystem->transformMat(spotlights[i].second);
			spotlights[i].first.direction = glm::normalize(mat[2]);
			spotlights[i].first.upDirection = glm::normalize(mat[1]);
			spotlights[i].first.rightDirection = glm::normalize(mat[0]);
			spotlights[i].first.position = mat[3] - glm::vec4(cameraPosition, 0.f);
			lightBuf.spotLights[i] = spotlights[i].first;
		}
		lightBuf.ambientColor = ambientColor;
		lightData.update(lightBuf);
		lightData.bind(LIGHTS_CBUF, Renderer::ShaderType::PS);
		ID3D11ShaderResourceView* textureView = spotlightMask.textureView.ptr();
		engine::d3d::s_devcon->PSSetShaderResources(1, 1, &textureView);
	}
	void LightSystem::addSunlight(const Sunlight& light, int transformID)
	{
		sunlights.push_back(std::pair<Sunlight, int>(light, transformID));
	}
	void LightSystem::addPointlight(const Pointlight& light, int transformID)
	{
		pointlights.push_back(std::pair<Pointlight, int>(light, transformID));
	}
	void LightSystem::addSpotlight(const Spotlight& light, int transformID)
	{
		spotlights.push_back(std::pair<Spotlight, int>(light, transformID));
	}
}