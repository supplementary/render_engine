#pragma once

#include "../include/win_def.hpp"

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <d3d11_4.h>
#include <d3dcompiler.h>

#include "../include/win_undef.hpp"

#include "DxRes.h"
#include <mutex>

namespace engine::d3d
{
	// global pointers to most used D3D11 objects for convenience:
	extern ID3D11Device5* s_device;
	extern ID3D11DeviceContext4* s_devcon;
	extern IDXGIFactory5* s_factory;
}

namespace engine
{
	class D3D // a singletone for accessing global rendering resources
	{
	public:
		D3D(D3D& other) = delete;
        void operator=(const D3D&) = delete;
		void init();
		void deinit();
        static D3D* getInstance();

	private:
		D3D() {};
		~D3D() {};

		inline static D3D* instance{ nullptr };
		inline static std::mutex mutex{};

		DxResPtr<IDXGIFactory> factory;
		DxResPtr<IDXGIFactory5> factory5;
		DxResPtr<ID3D11Device> device;
		DxResPtr<ID3D11Device5> device5;
		DxResPtr<ID3D11DeviceContext> devcon;
		DxResPtr<ID3D11DeviceContext4> devcon4;
		DxResPtr<ID3D11Debug> devdebug;
	};
}
