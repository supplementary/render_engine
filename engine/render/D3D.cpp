#include "D3D.h"

#include "assert.hpp"

namespace engine::d3d
{
	// global pointers to most used D3D11 objects for convenience:
	ID3D11Device5* s_device = nullptr;
	ID3D11DeviceContext4* s_devcon = nullptr;
	IDXGIFactory5* s_factory = nullptr;
}

// Say to NVidia or AMD driver to prefer a dedicated GPU instead of an integrated.
// This has effect on laptops.
extern "C"
{
	_declspec(dllexport) uint32_t NvOptimusEnablement = 1;
	_declspec(dllexport) uint32_t AmdPowerXpressRequestHighPerformance = 1;
}

namespace engine
{
	D3D* D3D::getInstance()
	{
		std::lock_guard<std::mutex> lock(mutex);
		if (instance == nullptr)
		{
			instance = new D3D();
		}
		return instance;
	}

	void D3D::init()
	{
		std::lock_guard<std::mutex> lock(mutex);
		HRESULT result;

		result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)factory.reset());
		ALWAYS_ASSERT(result >= 0 && "CreateDXGIFactory");

		result = factory->QueryInterface(__uuidof(IDXGIFactory5), (void**)factory5.reset());
		ALWAYS_ASSERT(result >= 0 && "Query IDXGIFactory5");

		{
			uint32_t index = 0;
			IDXGIAdapter1* adapter;
			while (factory5->EnumAdapters1(index++, &adapter) != DXGI_ERROR_NOT_FOUND)
			{
				DXGI_ADAPTER_DESC1 desc;
				adapter->GetDesc1(&desc);
				//LOG << "GPU #" << index << desc.Description;
			}
		}

		// Init D3D Device & Context

		const D3D_FEATURE_LEVEL featureLevelRequested = D3D_FEATURE_LEVEL_11_0;
		D3D_FEATURE_LEVEL featureLevelInitialized = D3D_FEATURE_LEVEL_11_0;
		result = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, D3D11_CREATE_DEVICE_DEBUG,
			&featureLevelRequested, 1, D3D11_SDK_VERSION, device.reset(), &featureLevelInitialized, devcon.reset());
		ALWAYS_ASSERT(result >= 0 && "D3D11CreateDevice");
		ALWAYS_ASSERT(featureLevelRequested == featureLevelInitialized && "D3D_FEATURE_LEVEL_11_0");

		result = device->QueryInterface(__uuidof(ID3D11Device5), (void**)device5.reset());
		ALWAYS_ASSERT(result >= 0 && "Query ID3D11Device5");

		result = devcon->QueryInterface(__uuidof(ID3D11DeviceContext4), (void**)devcon4.reset());
		ALWAYS_ASSERT(result >= 0 && "Query ID3D11DeviceContext4");

		result = device->QueryInterface(__uuidof(ID3D11Debug), (void**)devdebug.reset());
		ALWAYS_ASSERT(result >= 0 && "Query ID3D11Debug");

		// Write global pointers

		d3d::s_factory = factory5.ptr();
		d3d::s_device = device5.ptr();
		d3d::s_devcon = devcon4.ptr();
	}

	void D3D::deinit()
	{
		std::lock_guard<std::mutex> lock(mutex);
		d3d::s_factory = nullptr;
		d3d::s_device = nullptr;
		d3d::s_devcon = nullptr;
		factory.release();
		factory5.release();
		device.release();
		device5.release();
		devcon.release();
		devcon4.release();
		devdebug.release();
	}
}
