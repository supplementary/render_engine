#include "ShaderManager.h"

#include "assert.hpp"
#include "D3D.h"
#include "../source/Engine.h"

namespace render
{
    ShaderManager* ShaderManager::getInstance()
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (instance == nullptr)
        {
            instance = new ShaderManager();
        }
        return instance;
    }

    void ShaderManager::addVertexShader(std::string_view filename, bool inputLayout)
	{
        vertexShaders.insert({ std::string(filename), engine::DxResPtr<ID3D11VertexShader>() });

        UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
        flags |= D3DCOMPILE_DEBUG; // add more debug output
#endif
        ID3DBlob* vs_blob_ptr = nullptr, * error_blob = nullptr;

        HRESULT hr = D3DCompileFromFile(
            std::wstring(filename.begin(), filename.end()).c_str(),
            nullptr,
            D3D_COMPILE_STANDARD_FILE_INCLUDE,
            "vs_main",
            "vs_5_0",
            flags,
            0,
            &vs_blob_ptr,
            &error_blob);
        if (FAILED(hr)) {
            if (error_blob) {
                OutputDebugStringA((char*)error_blob->GetBufferPointer());
                error_blob->Release();
            }
            if (vs_blob_ptr) { vs_blob_ptr->Release(); }
            ALWAYS_ASSERT(false);
        }

        hr = engine::d3d::s_device->CreateVertexShader(
            vs_blob_ptr->GetBufferPointer(),
            vs_blob_ptr->GetBufferSize(),
            nullptr,
            vertexShaders[filename.data()].reset());
        ALWAYS_ASSERT(SUCCEEDED(hr));

        if (inputLayout)
            addInputLayout(filename, vs_blob_ptr);

        vs_blob_ptr->Release();
        if (error_blob) error_blob->Release();
	}

    void ShaderManager::addInputLayout(std::string_view filename, ID3DBlob* vsBlob)
    {
        inputLayouts.insert({ std::string(filename), engine::DxResPtr<ID3D11InputLayout>() });

        D3D11_INPUT_ELEMENT_DESC polygonLayout[10];
        memset(polygonLayout, 0, sizeof(polygonLayout));

        polygonLayout[0].SemanticName = "POSITION";
        polygonLayout[0].SemanticIndex = 0;
        polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
        polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

        polygonLayout[1].SemanticName = "TEXCOORD";
        polygonLayout[1].SemanticIndex = 0;
        polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
        polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
        polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

        polygonLayout[2].SemanticName = "NORMAL";
        polygonLayout[2].SemanticIndex = 0;
        polygonLayout[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
        polygonLayout[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
        polygonLayout[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

        polygonLayout[3].SemanticName = "TANGENT";
        polygonLayout[3].SemanticIndex = 0;
        polygonLayout[3].Format = DXGI_FORMAT_R32G32B32_FLOAT;
        polygonLayout[3].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
        polygonLayout[3].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

        polygonLayout[4].SemanticName = "BITANGENT";
        polygonLayout[4].SemanticIndex = 0;
        polygonLayout[4].Format = DXGI_FORMAT_R32G32B32_FLOAT;
        polygonLayout[4].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
        polygonLayout[4].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

        polygonLayout[5].SemanticName = "WORLD";
        polygonLayout[5].SemanticIndex = 0;
        polygonLayout[5].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
        polygonLayout[5].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
        polygonLayout[5].InputSlot = 1;
        polygonLayout[5].InstanceDataStepRate = 1;

        polygonLayout[6].SemanticName = "WORLD";
        polygonLayout[6].SemanticIndex = 1;
        polygonLayout[6].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
        polygonLayout[6].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
        polygonLayout[6].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
        polygonLayout[6].InputSlot = 1;
        polygonLayout[6].InstanceDataStepRate = 1;

        polygonLayout[7].SemanticName = "WORLD";
        polygonLayout[7].SemanticIndex = 2;
        polygonLayout[7].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
        polygonLayout[7].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
        polygonLayout[7].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
        polygonLayout[7].InputSlot = 1;
        polygonLayout[7].InstanceDataStepRate = 1;

        polygonLayout[8].SemanticName = "WORLD";
        polygonLayout[8].SemanticIndex = 3;
        polygonLayout[8].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
        polygonLayout[8].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
        polygonLayout[8].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
        polygonLayout[8].InputSlot = 1;
        polygonLayout[8].InstanceDataStepRate = 1;

        polygonLayout[9].SemanticName = "COLOR";
        polygonLayout[9].SemanticIndex = 0;
        polygonLayout[9].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
        polygonLayout[9].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
        polygonLayout[9].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
        polygonLayout[9].InputSlot = 1;
        polygonLayout[9].InstanceDataStepRate = 1;

        // Get a count of the elements in the layout.
        unsigned numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

        // Create the vertex input layout.
        HRESULT hr = engine::d3d::s_device->CreateInputLayout(polygonLayout, numElements,
            vsBlob->GetBufferPointer(),
            vsBlob->GetBufferSize(),
            inputLayouts[filename.data()].reset());
        ALWAYS_ASSERT(SUCCEEDED(hr));
    }

    void ShaderManager::addPixelShader(std::string_view filename)
    {
        pixelShaders.insert({ std::string(filename), engine::DxResPtr<ID3D11PixelShader>() });

        UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
        flags |= D3DCOMPILE_DEBUG;
#endif
        ID3DBlob* ps_blob_ptr = nullptr, * error_blob = nullptr;

        HRESULT hr = D3DCompileFromFile(
            std::wstring(filename.begin(), filename.end()).c_str(),
            nullptr,
            D3D_COMPILE_STANDARD_FILE_INCLUDE,
            "ps_main",
            "ps_5_0",
            flags,
            0,
            &ps_blob_ptr,
            &error_blob);
        if (FAILED(hr)) {
            if (error_blob) {
                OutputDebugStringA((char*)error_blob->GetBufferPointer());
                error_blob->Release();
            }
            if (ps_blob_ptr) { ps_blob_ptr->Release(); }
            ALWAYS_ASSERT(false);
        }

        hr = engine::d3d::s_device->CreatePixelShader(
            ps_blob_ptr->GetBufferPointer(),
            ps_blob_ptr->GetBufferSize(),
            nullptr,
            pixelShaders[filename.data()].reset());
        ALWAYS_ASSERT(SUCCEEDED(hr));

        ps_blob_ptr->Release();
        if (error_blob) error_blob->Release();
    }

    void ShaderManager::init()
    {
    }

    void ShaderManager::deinit()
    {
        std::lock_guard<std::mutex> lock(mutex);
        inputLayouts.clear();
        vertexShaders.clear();
        pixelShaders.clear();
    }

    void ShaderManager::bindVS(std::string_view filename)
    {
        if (vertexShaders.find(filename.data()) == vertexShaders.end())
            addVertexShader(filename.data());
        if (inputLayouts.find(filename.data()) != inputLayouts.end())
            engine::d3d::s_devcon->IASetInputLayout(inputLayouts.at(filename.data()));
        else
            engine::d3d::s_devcon->IASetInputLayout(NULL);
        engine::d3d::s_devcon->VSSetShader(vertexShaders.at(filename.data()), NULL, 0);
    }
    void ShaderManager::bindPS(std::string_view filename)
    {
        if (pixelShaders.find(filename.data()) == pixelShaders.end())
            addPixelShader(filename.data());
        engine::d3d::s_devcon->PSSetShader(pixelShaders.at(filename.data()), NULL, 0);
    }
}
