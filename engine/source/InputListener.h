#pragma once

enum M_BUTTON {LMB = 1, RMB = 2, MMB = 16};
enum KB_BUTTON {
	W = 0x57, A = 0x41, S = 0x53, D = 0x44, Q = 0x51, E = 0x45, F = 0x46, SHIFT = 0x10, CTRL = 0x11, SPACE = 0x20, ESC = 0x1B, NUM_PLUS = 0x6B, NUM_MINUS = 0x6D,
	PLUS = 0xBB, MINUS = 0xBD, LEFT_ARROW = 0x25, UP_ARROW = 0x26, RIGHT_ARROW = 0x27, DOWN_ARROW = 0x28
};

class InputListener
{
public:
	InputListener() {}
	virtual void onKbButtonDown(KB_BUTTON b) {};
	virtual void onKbButtonUp(KB_BUTTON b) {};
	virtual void onMouseDown(M_BUTTON m) {};
	virtual void onMouseUp(M_BUTTON m) {};
	virtual void onMouseWheel(int delta) {};
	virtual void onMouseMove(int vKey, float posX, float posY) {};
	virtual void onWindowResize(int width, int height) {};
	virtual void onQuit() {};
};