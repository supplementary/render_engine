#pragma once

#define GLM_FORCE_INLINE
#include "glm.hpp"
#include "gtc/quaternion.hpp"
#include "math/MathUtils.h"

namespace math
{
	class Camera
	{
	public:
		glm::vec3 right() 		const { return glm::row(viewInv, 0); }
		glm::vec3 top() 		const { return glm::row(viewInv, 1); }
		glm::vec3 forward() 	const { return glm::row(viewInv, 2); }
		glm::vec3 position()	const { return glm::row(viewInv, 3); }

		void setPerspective(float fov, float aspect, float n, float f);
		void setWorldOffset(const glm::vec3& offset);
		void addWorldOffset(const glm::vec3& offset);
		void addRelativeOffset(const glm::vec3& offset);
		void setWorldAngles(const math::Angles& angles);
		void addWorldAngles(const math::Angles& angles);
		void addRelativeAngles(const math::Angles& angles);
		void updateBasis();
		void updateMatrices();
		inline glm::vec4 viewProjection(const glm::vec4& v) const { return v * viewProj; }
		inline const glm::mat4& viewProjectionMatrix() const { return viewProj; }
		inline const glm::mat4& viewProjectionCCMatrix() const { return viewProjCameraCentered; }
		inline const glm::mat4& viewInvMatrix() const { return viewInv; }
		inline glm::vec4 viewProjectionInv(const glm::vec4& v) const { auto u = v * viewProjInv; return u / u.w; }
		inline glm::vec4 projectionInv(const glm::vec4& v) const { auto u = v * projInv; return u / u.w; }
		math::Angles angles;
	protected:
		void normalizeRotation(bool force = false);

		glm::mat4 view = glm::mat4(1.f);
		glm::mat4 proj = glm::mat4(1.f);
		glm::mat4 viewProj = glm::mat4(1.f);
		glm::mat4 viewProjCameraCentered = glm::mat4(1.f);

		glm::mat4 viewInv = glm::mat4(1.f);
		glm::mat4 projInv = glm::mat4(1.f);
		glm::mat4 viewProjInv = glm::mat4(1.f);

		glm::quat rotation = glm::quat(1.f, 0.f, 0.f, 0.f);
		

		bool updatedBasis = false;
		bool updatedMatrices = false;

		int rotationNormalizeCounter = 0;
	};
};