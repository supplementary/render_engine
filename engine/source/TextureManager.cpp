#include "TextureManager.h"

#include "../DXTex/DDSTextureLoader11.h"
#include "../render/D3D.h"

namespace engine
{
	void TextureManager::deinit()
	{
		std::lock_guard<std::mutex> lock(mutex);
		textureResources.clear();
		textureViews.clear();
	}

	Texture TextureManager::texture(std::string_view name)
	{
		auto textureView = textureViews.find(name.data());
		if (textureView == textureViews.end())
			loadTexture(name);
		Texture texture = { textureViews[name.data()] };
		return texture;
	}

	TextureManager* engine::TextureManager::getInstance()
	{
		std::lock_guard<std::mutex> lock(mutex);
		if (instance == nullptr)
		{
			instance = new TextureManager();
		}
		return instance;
	}
	void TextureManager::loadTexture(std::string_view path)
	{
		HRESULT hr = DirectX::CreateDDSTextureFromFile(
			d3d::s_device,
			std::wstring(path.begin(), path.end()).c_str(),
			textureResources[path.data()].reset(),
			textureViews[path.data()].reset()
			);
		ALWAYS_ASSERT(SUCCEEDED(hr));
	}
}