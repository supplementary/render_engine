#pragma once

#include "glm.hpp"

namespace math
{
	struct Ray
	{
		glm::vec3 origin;
		glm::vec3 direction;

		void set(const glm::vec3& origin, const glm::vec3& direction)
		{
			this->origin = origin;
			this->direction = glm::normalize(direction);
		}
	};

	struct Intersection
	{
		float t;
		glm::vec3 pos;
		glm::vec3 normal;

		bool valid() const { return std::isfinite(t); }
		void reset() { t = std::numeric_limits<float>::infinity(); }
	};

}