#pragma once

#include "glm.hpp"
#include <algorithm>
#include "Ray.h"
#include "MathUtils.h"

namespace math
{
	struct Box
	{
		glm::vec3 min;
		glm::vec3 max;

		static constexpr float Inf = std::numeric_limits<float>::infinity();
		static constexpr Box empty() { return  { { Inf, Inf, Inf }, { -Inf, -Inf, -Inf } }; }
		static constexpr Box unit() { return  { { -1.f, -1.f, -1.f }, { 1.f, 1.f, 1.f } }; }

		glm::vec3 size() const { return max - min; }
		glm::vec3 center() const { return (min + max) / 2.f; }
		float radius() const { return glm::length(size()) / 2.f; }

		void reset()
		{
			constexpr float maxf = std::numeric_limits<float>::max();
			min = { maxf , maxf , maxf };
			max = -min;
		}

		void expand(const Box& other)
		{
			min = glm::min(min, other.min);
			max = glm::max(max, other.max);
		}

		void expand(const glm::vec3& point)
		{
			min = glm::min(min, point);
			max = glm::max(max, point);
		}

		bool contains(const glm::vec3& P) const
		{
			return
				min[0] <= P[0] && P[0] <= max[0] &&
				min[1] <= P[1] && P[1] <= max[1] &&
				min[2] <= P[2] && P[2] <= max[2];
		}

		bool intersect(const math::Ray& ray, float boxT) const
		{
			float t1, t2, rdx;
			float tmin = -std::numeric_limits<float>::infinity();
			float tmax = std::numeric_limits<float>::infinity();
			for (int i = 0; i < 3; i++)
			{
				if (!math::isAlmostZero(ray.direction[i]))
				{
					rdx = 1.0f / ray.direction[i];
					t1 = (min[i] - ray.origin[i]) * rdx;
					t2 = (max[i] - ray.origin[i]) * rdx;
					tmin = std::max(tmin, std::min(t1, t2));
					tmax = std::min(tmax, std::max(t1, t2));
				}
				else if (min[i] >= ray.origin[i] || max[i] <= ray.origin[i])
					return false;
			}
			if (tmax > 0 && tmin < tmax && tmin < boxT)
				return true;
			return false;
		}
	};
}
