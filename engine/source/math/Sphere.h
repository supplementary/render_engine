#pragma once

#include "glm.hpp"
#include "Ray.h"

namespace math
{
	struct Sphere
	{
		float radius;
		glm::vec3 center;

		inline bool intersect(const Ray& ray, math::Intersection& outNearest) const
		{
			// Using double precision because of visual artifacts
			glm::dvec3 oc = center - ray.origin;
			double b = glm::dot(oc, (glm::dvec3)ray.direction);
			double c = glm::dot(oc, oc) - radius * radius;
			float discriminant = (float)(b * b - c);

			if (discriminant < 0)
				return false;
			float t = (float)b - sqrtf(discriminant);
			if (t > 0 && t < outNearest.t)
			{
				outNearest.t = t;
				outNearest.pos = ray.origin + ray.direction * outNearest.t;
				outNearest.normal = glm::normalize(outNearest.pos - center);
				return true;
			}
			return false;
		}
	};
}