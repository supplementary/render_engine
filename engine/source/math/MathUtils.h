#pragma once

#define GLM_FORCE_INLINE
#include "glm.hpp"
#include "gtc/quaternion.hpp"
#include "gtc/matrix_access.hpp"

namespace math
{
	constexpr float EPSILON = 1e-6f;
	
	struct Angles
	{
		float roll;
		float pitch;
		float yaw;
	};

	glm::mat4 toMat(const glm::vec3& position, const glm::vec3& scale, const glm::quat& rotation);
	inline bool areAlmostEqual(float x, float y) { return std::fabs(x - y) < EPSILON; }
	inline bool isPositive(float x) { return x > EPSILON; }
	inline bool isNegative(float x) { return x < -EPSILON; }
	inline bool isAlmostZero(float x) { return std::fabs(x) < EPSILON; }

	// We can invert an orthonormal transformation matrix (it has no scaling and no skewing) without calling more heavy invert()
	void invertOrthonormal(const glm::mat4& src, glm::mat4& dst);

	// We can invert an orthogonal transformation matrix (it may have scaling and has no skewing) without calling more heavy invert()
	void invertOrthogonal(const glm::mat4& src, glm::mat4& dst);
};