#pragma once

#include "glm.hpp"
#include "Ray.h"
#include "MathUtils.h"

namespace math
{
	struct Plane
	{
		glm::vec3 normal;
		glm::vec3 position;

		inline bool intersect(const Ray& ray, math::Intersection& outNearest) const
		{
			float denom = glm::dot(normal, ray.direction);
			if (!math::isNegative(denom))
				return false;
			float t = (glm::dot(position - ray.origin, normal)) / denom;
			if (t > 0 && t < outNearest.t)
			{
				outNearest.t = t;
				outNearest.pos = ray.origin + ray.direction * t;
				outNearest.normal = normal;
				return true;
			}
			return false;
		}
	};
};