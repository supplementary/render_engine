#include "MathUtils.h"

namespace math
{
	glm::mat4 toMat(const glm::vec3& position, const glm::vec3& scale, const glm::quat& rotation)
	{
		glm::mat4 rotmat(glm::normalize(rotation));
		
		glm::mat4 scalemat(1.f);
		scalemat[0][0] = scale[0];
		scalemat[1][1] = scale[1];
		scalemat[2][2] = scale[2];

		glm::mat4 trmat(1.f);
		trmat[3][0] = position.x;
		trmat[3][1] = position.y;
		trmat[3][2] = position.z;

		return trmat * rotmat * scalemat;
	}

	void invertOrthonormal(const glm::mat4& src, glm::mat4& dst)
	{
		dst = glm::transpose(src);
		const glm::vec3& pos = dst[3];

		dst = glm::row(dst, 3,
			-pos[0] * src[0]
			- pos[1] * src[1]
			- pos[2] * src[2]);

		dst[3] = { 0.f, 0.f, 0.f, 1.f };
	}

	void invertOrthogonal(const glm::mat4& src, glm::mat4& dst)
	{
		dst = glm::transpose(src);		

		glm::vec4 invLengthsXYZ = { 1.f / glm::length(glm::vec3(dst[0])), 1.f / glm::length(glm::vec3(dst[1])), 1.f / glm::length(glm::vec3(dst[2])), 1.f };

		for (int col = 0; col < 3; col++)
		{
			for (int row = 0; row < 3; row++)
			{
				dst[col][row] *= invLengthsXYZ[col];
			}
		}

		const glm::vec3& pos = dst[3];

		dst = glm::row(dst, 3,
			(-pos[0] * src[0]
				- pos[1] * src[1]
				- pos[2] * src[2]) * invLengthsXYZ);

		dst[3] = { 0.f, 0.f, 0.f, 1.f };
		glm::mat4 invScaleMat(1.f);
		invScaleMat[0][0] = invLengthsXYZ[0];
		invScaleMat[1][1] = invLengthsXYZ[1];
		invScaleMat[2][2] = invLengthsXYZ[2];
		dst *= invScaleMat;
	}
}