#pragma once

#include "Mesh.h"
#include "../render/Buffers.h"
#include "math/Box.h"
#include <unordered_map>
#include <string>
#include <memory>

namespace engine
{
	class Model
	{
	public:
		struct MeshRange
		{
			uint32_t vertexOffset; // offset in vertices
			uint32_t indexOffset; // offset in indices
			uint32_t vertexNum; // num of vertices
			uint32_t indexNum; // num of indices
		};
	
		std::string name;
		std::vector<Mesh> meshes;
		math::Box box;
		std::vector<MeshRange> ranges; // where each mesh data is stored in vertices
		VertexBuffer<Mesh::Vertex> vertices; // stores vertices of all meshes of this Model
		IndexBuffer indices; // stores vertex indices of all meshes of this Model
	};

	class ModelManager
	{
	public:
		ModelManager(ModelManager& other) = delete;
		void operator=(const ModelManager&) = delete;
		void deinit();
		void init();
		std::shared_ptr<Model> model(std::string_view name);
		static ModelManager* getInstance();

	protected:
		void loadModel(std::string_view path);
		void initUnitSphere();
		void initUnitSphereFlat();
		std::unordered_map<std::string, std::shared_ptr<Model>> models;

	private:
		ModelManager() {};
		~ModelManager() {};

		inline static ModelManager* instance{ nullptr };
		inline static std::mutex mutex{};
	};

}