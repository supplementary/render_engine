#pragma once

#include "glm.hpp"
#include "math/Ray.h"
#include "math/Box.h"
#include "math/MathUtils.h"
#include "TriangleOctree.h"
#include <vector>
#include <array>
#include <string>

namespace engine
{
	struct MeshIntersection
	{
		glm::vec3 pos;
		glm::vec3 normal;
		float t;
		uint32_t triangle;

		bool valid() const { return std::isfinite(t); }
		void reset() { t = std::numeric_limits<float>::infinity(); }
	};

	class Mesh
	{
	public:
		struct Vertex
		{
			glm::vec3 pos;
			glm::vec2 tc;
			glm::vec3 normal;
			glm::vec3 tangent;
			glm::vec3 bitangent;
		};

		struct Triangle
		{
			std::array<int, 3> indices;
			static bool intersect(const math::Ray& ray, MeshIntersection& nearest, const glm::vec3& V1, const glm::vec3& V2, const glm::vec3& V3)
			{
				const float EPSILON = 1e-6f;
				glm::vec3 edge1, edge2, h, s, q;
				float det, f, u, v;
				edge1 = V2 - V1;
				edge2 = V3 - V1;
				h = glm::cross(ray.direction, edge2);
				det = glm::dot(edge1, h);

				if (!math::isPositive(det))
					return false;

				f = 1.f / det;
				s = ray.origin - V1;
				u = f * glm::dot(s, h);

				if (u < 0.f || u > 1.f)
					return false;

				q = glm::cross(s, edge1);
				v = f * glm::dot(ray.direction, q);

				if (v < 0.f || v + u > 1.f)
					return false;

				float t = f * glm::dot(edge2, q);

				if (t > 0 && t < nearest.t)
				{
					nearest.t = t;
					nearest.normal = glm::normalize(glm::cross(edge1, edge2));
					nearest.pos = ray.origin + ray.direction * t;
					return true;
				}

				return false;
			}
		};

		Mesh() {}

		Mesh(const std::vector<Vertex>& vertices, const std::vector<Triangle>& triangles)
			: vertices(vertices), triangles(triangles)
		{
			box.reset();
			for (auto& v : vertices)
			{
				box.expand(v.pos);
			}
			octree.initialize(*this);
		}

		Mesh(Mesh& other)
		{
			vertices = other.vertices;
			triangles = other.triangles;
			box = other.box;
			octree.initialize(*this);
		}

		Mesh(const Mesh& other)
		{
			vertices = other.vertices;
			triangles = other.triangles;
			box = other.box;
			octree.initialize(*this);
		}

		bool intersect(const math::Ray& ray, MeshIntersection& outNearest) const
		{
			return octree.intersect(ray, outNearest);
		}

		void updateOctree()
		{
			octree.initialize(*this);
		}

		std::string name;
		std::vector<Vertex> vertices;
		std::vector<Triangle> triangles;
		math::Box box;
		std::vector<glm::mat4> instances;
		std::vector<glm::mat4> instancesInv;

	protected:
		TriangleOctree octree;
	};
}
