#pragma once

#include "../render/DxRes.h"
#include "../render/Buffers.h"
#include <unordered_map>
#include <string>
#include <memory>

namespace engine
{
	struct Texture
	{
		DxResPtr<ID3D11ShaderResourceView> textureView;
	};

	class TextureManager
	{
	public:
		TextureManager(TextureManager& other) = delete;
		void operator=(const TextureManager&) = delete;
		void deinit();
		void init() {}
		Texture texture(std::string_view name);
		static TextureManager* getInstance();

	protected:
		void loadTexture(std::string_view path);
		std::unordered_map<std::string, DxResPtr<ID3D11Resource>> textureResources;
		std::unordered_map<std::string, DxResPtr<ID3D11ShaderResourceView>> textureViews;

	private:
		TextureManager() {};
		~TextureManager() {};

		inline static TextureManager* instance{ nullptr };
		inline static std::mutex mutex{};
	};

}