#pragma once

#include "../render/D3D.h"
#include "../render/ShaderManager.h"
#include "../source/TextureManager.h"
#include "../source/ModelManager.h"
#include "../render/Buffers.h"
#include "../render/Renderer.h"
#include "../render/MeshSystem.h"
#include "../source/TransformSystem.h"
#include "../render/LightSystem.h"

namespace engine
{
	struct PerFrameConstantBuffer
	{
		float mainResolution[4];
		float mousePosition[4];
		float time;
		float EV100;
		float padding[2];
	};
	class Engine
	{
	public:
		// called from main.cpp
		static void init()
		{
			// initilizes engine singletons
			D3D::getInstance()->init();
			render::ShaderManager::getInstance()->init();
			render::Renderer::getInstance()->init();
			ModelManager::getInstance()->init();
			TextureManager::getInstance()->init();
			render::MeshSystem::getInstance()->init();
			TransformSystem::getInstance()->init();
			render::LightSystem::getInstance()->init();
		}

		static void deinit()
		{
			// deinitilizes engine singletons in reverse order
			render::LightSystem::getInstance()->deinit();
			TransformSystem::getInstance()->deinit();
			render::MeshSystem::getInstance()->deinit();
			TextureManager::getInstance()->deinit();
			ModelManager::getInstance()->deinit();
			render::Renderer::getInstance()->deinit();
			render::ShaderManager::getInstance()->deinit();
			D3D::getInstance()->deinit();
		}

		inline static PerFrameConstantBuffer g_frameCBuffer;
	};
}