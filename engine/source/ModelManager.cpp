#include "ModelManager.h"

#include "assimp/Importer.hpp"
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <functional>

namespace engine
{
    void ModelManager::deinit()
    {
        std::lock_guard<std::mutex> lock(mutex);
        models.clear();
    }

	void ModelManager::init()
	{
		initUnitSphere();
		initUnitSphereFlat();
	}

	std::shared_ptr<Model> ModelManager::model(std::string_view name)
	{
		auto model = models.find(name.data());
		if (model == models.end())
			loadModel(name);
		else
			return model->second;
		return models[name.data()];
	}

	ModelManager* engine::ModelManager::getInstance()
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (instance == nullptr)
        {
            instance = new ModelManager();
        }
        return instance;
    }
    void ModelManager::loadModel(std::string_view path)
    {
		// Load aiScene

		uint32_t flags = uint32_t(aiProcess_Triangulate | aiProcess_GenBoundingBoxes | aiProcess_ConvertToLeftHanded | aiProcess_CalcTangentSpace);
		// aiProcess_Triangulate - ensure that all faces are triangles and not polygonals, otherwise triangulare them
		// aiProcess_GenBoundingBoxes - automatically compute bounding box, though we could do that manually
		// aiProcess_ConvertToLeftHanded - Assimp assumes left-handed basis orientation by default, convert for Direct3D
		// aiProcess_CalcTangentSpace - computes tangents and bitangents, they are used in advanced lighting

		Assimp::Importer importer;
		const aiScene* assimpScene = importer.ReadFile(path.data(), flags);
		DEV_ASSERT(assimpScene);

		uint32_t numMeshes = assimpScene->mNumMeshes;

		// Load vertex data

		std::shared_ptr<Model> model = std::make_shared<Model>();
		models[path.data()] = model;
		model->name = path;
		model->box = {};
		model->meshes.resize(numMeshes);
		model->ranges.resize(numMeshes);

		static_assert(sizeof(glm::vec3) == sizeof(aiVector3D));
		static_assert(sizeof(Mesh::Triangle) == 3 * sizeof(uint32_t));

		int indexOffset = 0;
		int vertexOffset = 0;
		std::vector<Mesh::Vertex> vertices;
		std::vector<unsigned> indices;

		for (uint32_t i = 0; i < numMeshes; ++i)
		{
			auto& srcMesh = assimpScene->mMeshes[i];
			auto& dstMesh = model->meshes[i];
			auto& dstMeshRange = model->ranges[i];

			dstMesh.name = srcMesh->mName.C_Str();
			dstMesh.box.min = reinterpret_cast<glm::vec3&>(srcMesh->mAABB.mMin);
			dstMesh.box.max = reinterpret_cast<glm::vec3&>(srcMesh->mAABB.mMax);

			dstMesh.vertices.resize(srcMesh->mNumVertices);
			dstMesh.triangles.resize(srcMesh->mNumFaces);

			dstMeshRange.indexNum = 3 * srcMesh->mNumFaces;
			dstMeshRange.indexOffset = indexOffset;
			dstMeshRange.vertexNum = srcMesh->mNumVertices;
			dstMeshRange.vertexOffset = vertexOffset;
			indexOffset += dstMeshRange.indexNum;
			vertexOffset += dstMeshRange.vertexNum;

			for (uint32_t v = 0; v < srcMesh->mNumVertices; ++v)
			{
				Mesh::Vertex& vertex = dstMesh.vertices[v];
				vertex.pos = reinterpret_cast<glm::vec3&>(srcMesh->mVertices[v]);
				vertex.tc = reinterpret_cast<glm::vec2&>(srcMesh->mTextureCoords[0][v]);
				vertex.normal = reinterpret_cast<glm::vec3&>(srcMesh->mNormals[v]);
				vertex.tangent = reinterpret_cast<glm::vec3&>(srcMesh->mTangents[v]);
				vertex.bitangent = reinterpret_cast<glm::vec3&>(srcMesh->mBitangents[v]) * -1.f; // Flip V

				vertices.push_back(vertex);
			}

			for (uint32_t f = 0; f < srcMesh->mNumFaces; ++f)
			{
				const auto& face = srcMesh->mFaces[f];
				DEV_ASSERT(face.mNumIndices == 3);
				dstMesh.triangles[f] = *reinterpret_cast<Mesh::Triangle*>(face.mIndices);

				indices.push_back(face.mIndices[0]);
				indices.push_back(face.mIndices[1]);
				indices.push_back(face.mIndices[2]);
			}
			
			dstMesh.updateOctree();
		}
		model->vertices.init(vertices, D3D11_USAGE_IMMUTABLE);
		model->indices.init(indices, D3D11_USAGE_IMMUTABLE);

		// Recursively load mesh instances (meshToModel transformation matrices)

		std::function<void(aiNode*)> loadInstances;
		loadInstances = [&loadInstances, &model](aiNode* node)
			{
				const glm::mat4 nodeToParent = reinterpret_cast<const glm::mat4&>(node->mTransformation.Transpose());
				const glm::mat4 parentToNode = glm::inverse(nodeToParent);

				// The same node may contain multiple meshes in its space, referring to them by indices
				for (uint32_t i = 0; i < node->mNumMeshes; ++i)
				{
					uint32_t meshIndex = node->mMeshes[i];
					model->meshes[meshIndex].instances.push_back(nodeToParent);
					model->meshes[meshIndex].instancesInv.push_back(parentToNode);
				}

				for (uint32_t i = 0; i < node->mNumChildren; ++i)
					loadInstances(node->mChildren[i]);
			};

		loadInstances(assimpScene->mRootNode);
    }

	void ModelManager::initUnitSphere()
	{
		const uint32_t SIDES = 6;
		const uint32_t GRID_SIZE = 12;
		const uint32_t TRIS_PER_SIDE = GRID_SIZE * GRID_SIZE * 2;
		const uint32_t VERT_PER_SIZE = (GRID_SIZE + 1) * (GRID_SIZE + 1);

		std::shared_ptr<Model> model = std::make_shared<Model>();
		models["UNIT_SPHERE"] = model;
		model->name = "UNIT_SPHERE";
		model->box = math::Box::unit();

		Model::MeshRange& meshRange = model->ranges.emplace_back();
		meshRange.vertexOffset = 0;
		meshRange.indexOffset = 0;
		meshRange.vertexNum = VERT_PER_SIZE * SIDES;
		meshRange.indexNum = TRIS_PER_SIDE * SIDES * 3;

		Mesh& mesh = model->meshes.emplace_back();
		mesh.name = "UNIT_SPHERE";
		mesh.box = model->box;
		mesh.instances = { glm::mat4(1.f) };
		mesh.instancesInv = { glm::mat4(1.f) };

		mesh.vertices.resize(VERT_PER_SIZE * SIDES);
		Mesh::Vertex* vertex = mesh.vertices.data();

		std::vector<unsigned> indices;

		int sideMasks[6][3] =
		{
			{ 2, 1, 0 },
			{ 0, 1, 2 },
			{ 2, 1, 0 },
			{ 0, 1, 2 },
			{ 0, 2, 1 },
			{ 0, 2, 1 }
		};

		float sideSigns[6][3] =
		{
			{ +1, +1, +1 },
			{ -1, +1, +1 },
			{ -1, +1, -1 },
			{ +1, +1, -1 },
			{ +1, -1, -1 },
			{ +1, +1, +1 }
		};

		for (int side = 0; side < SIDES; ++side)
		{
			for (int row = 0; row < GRID_SIZE + 1; ++row)
			{
				for (int col = 0; col < GRID_SIZE + 1; ++col)
				{
					glm::vec3 v;
					v.x = col / float(GRID_SIZE) * 2.f - 1.f;
					v.y = row / float(GRID_SIZE) * 2.f - 1.f;
					v.z = 1.f;

					vertex[0].pos[sideMasks[side][0]] = v.x * sideSigns[side][0];
					vertex[0].pos[sideMasks[side][1]] = v.y * sideSigns[side][1];
					vertex[0].pos[sideMasks[side][2]] = v.z * sideSigns[side][2];
					vertex[0].normal = vertex[0].pos = glm::normalize(vertex[0].pos);

					vertex += 1;
				}
			}
		}

		mesh.triangles.resize(TRIS_PER_SIDE * SIDES);
		auto* triangle = mesh.triangles.data();

		for (int side = 0; side < SIDES; ++side)
		{
			uint32_t sideOffset = VERT_PER_SIZE * side;

			for (int row = 0; row < GRID_SIZE; ++row)
			{
				for (int col = 0; col < GRID_SIZE; ++col)
				{
					triangle[0].indices[0] = sideOffset + (row + 0) * (GRID_SIZE + 1) + col + 0;
					triangle[0].indices[1] = sideOffset + (row + 1) * (GRID_SIZE + 1) + col + 0;
					triangle[0].indices[2] = sideOffset + (row + 0) * (GRID_SIZE + 1) + col + 1;

					triangle[1].indices[0] = sideOffset + (row + 1) * (GRID_SIZE + 1) + col + 0;
					triangle[1].indices[1] = sideOffset + (row + 1) * (GRID_SIZE + 1) + col + 1;
					triangle[1].indices[2] = sideOffset + (row + 0) * (GRID_SIZE + 1) + col + 1;

					indices.push_back(triangle[0].indices[0]);
					indices.push_back(triangle[0].indices[1]);
					indices.push_back(triangle[0].indices[2]);
					indices.push_back(triangle[1].indices[0]);
					indices.push_back(triangle[1].indices[1]);
					indices.push_back(triangle[1].indices[2]);

					triangle += 2;
				}
			}
		}

		mesh.updateOctree();

		model->vertices.init(mesh.vertices, D3D11_USAGE_IMMUTABLE);
		model->indices.init(indices, D3D11_USAGE_IMMUTABLE);
	}

	void ModelManager::initUnitSphereFlat()
	{
		const uint32_t SIDES = 6;
		const uint32_t GRID_SIZE = 12;
		const uint32_t TRIS_PER_SIDE = GRID_SIZE * GRID_SIZE * 2;
		const uint32_t VERT_PER_SIZE = 3 * TRIS_PER_SIDE;

		std::shared_ptr<Model> model = std::make_shared<Model>();
		models["UNIT_SPHERE_FLAT"] = model;
		model->name = "UNIT_SPHERE_FLAT";
		model->box = math::Box::unit();

		Model::MeshRange& meshRange = model->ranges.emplace_back();
		meshRange.vertexOffset = 0;
		meshRange.indexOffset = 0;
		meshRange.vertexNum = TRIS_PER_SIDE * SIDES * 3;
		meshRange.indexNum = TRIS_PER_SIDE * SIDES * 3;

		engine::Mesh& mesh = model->meshes.emplace_back();
		mesh.name = "UNIT_SPHERE_FLAT";
		mesh.box = model->box;
		mesh.instances = { glm::mat4(1.f) };
		mesh.instancesInv = { glm::mat4(1.f) };

		mesh.vertices.resize(VERT_PER_SIZE * SIDES);
		Mesh::Vertex* vertex = mesh.vertices.data();

		std::vector<unsigned> indices;

		int sideMasks[6][3] =
		{
			{ 2, 1, 0 },
			{ 0, 1, 2 },
			{ 2, 1, 0 },
			{ 0, 1, 2 },
			{ 0, 2, 1 },
			{ 0, 2, 1 }
		};

		float sideSigns[6][3] =
		{
			{ +1, +1, +1 },
			{ -1, +1, +1 },
			{ -1, +1, -1 },
			{ +1, +1, -1 },
			{ +1, -1, -1 },
			{ +1, +1, +1 }
		};
		int index = 0;
		for (int side = 0; side < SIDES; ++side)
		{
			for (int row = 0; row < GRID_SIZE; ++row)
			{
				for (int col = 0; col < GRID_SIZE; ++col)
				{
					float left = (col + 0) / float(GRID_SIZE) * 2.f - 1.f;
					float right = (col + 1) / float(GRID_SIZE) * 2.f - 1.f;
					float bottom = (row + 0) / float(GRID_SIZE) * 2.f - 1.f;
					float top = (row + 1) / float(GRID_SIZE) * 2.f - 1.f;

					glm::vec3 quad[4] =
					{
						{ left, bottom, 1.f },
						{ left, top, 1.f },
						{ right, bottom, 1.f },
						{ right, top, 1.f }
					};

					auto setPos = [sideMasks, sideSigns](int side, Mesh::Vertex& dst, const glm::vec3& pos)
					{
						dst.pos[sideMasks[side][0]] = pos.x * sideSigns[side][0];
						dst.pos[sideMasks[side][1]] = pos.y * sideSigns[side][1];
						dst.pos[sideMasks[side][2]] = pos.z * sideSigns[side][2];
						dst.pos = glm::normalize(dst.pos);
					};

					setPos(side, vertex[0], quad[0]);
					setPos(side, vertex[1], quad[1]);
					setPos(side, vertex[2], quad[2]);

					{
						glm::vec3 AB = vertex[1].pos - vertex[0].pos;
						glm::vec3 AC = vertex[2].pos - vertex[0].pos;
						vertex[0].normal = vertex[1].normal = vertex[2].normal = glm::normalize(glm::cross(AB, AC));
					}

					vertex += 3;

					setPos(side, vertex[0], quad[1]);
					setPos(side, vertex[1], quad[3]);
					setPos(side, vertex[2], quad[2]);

					{
						glm::vec3 AB = vertex[1].pos - vertex[0].pos;
						glm::vec3 AC = vertex[2].pos - vertex[0].pos;
						vertex[0].normal = vertex[1].normal = vertex[2].normal = glm::normalize(glm::cross(AB, AC));
					}

					vertex += 3;

					indices.push_back(index);
					indices.push_back(index+1);
					indices.push_back(index+2);
					indices.push_back(index+3);
					indices.push_back(index+4);
					indices.push_back(index+5);
					index += 6;
				}
			}
		}

		mesh.updateOctree();

		model->vertices.init(mesh.vertices, D3D11_USAGE_IMMUTABLE);
		model->indices.init(indices, D3D11_USAGE_IMMUTABLE);
	}
}