#include "TransformSystem.h"

namespace engine
{
	TransformSystem* TransformSystem::getInstance()
	{
		std::lock_guard<std::mutex> lock(mutex);
		if (instance == nullptr)
		{
			instance = new TransformSystem();
		}
		return instance;
	}
	TransformSystem::Transform& TransformSystem::transform(SolidVector<Transform>::ID id)
	{
		return transforms[id];
	}
	glm::mat4 TransformSystem::transformMat(SolidVector<Transform>::ID id) const
	{
		glm::mat4 mat(1.f);
		Transform t;
		while (id < transforms.size() && transforms.occupied(id))
		{
			t = transforms[id];
			mat = t.transformMat * mat;
			id = t.parentTransformID;
		}
		return mat;
	}
	SolidVector<TransformSystem::Transform>::ID TransformSystem::insert(const glm::vec3& position, const glm::vec3& scale, const math::Angles& angles, SolidVector<Transform>::ID parentTransformID)
	{
		Transform transform;
		transform.parentTransformID = parentTransformID;
		glm::quat rotation = glm::quat({ angles.pitch, -angles.yaw, angles.roll });
		transform.transformMat = math::toMat(position, scale, rotation);
		return transforms.insert(transform);
	}
}
