#include "Camera.h"

#include "gtc/matrix_access.hpp"

namespace math
{
	// Sets proj and projInv
	void Camera::setPerspective(float fov, float aspect, float n, float f)
	{
		float ctg = glm::tan((glm::pi<float>() - fov) * 0.5f);
		proj[0][0] = ctg / aspect;
		proj[1][1] = ctg;
		proj[2][2] = n / (n - f);
		proj[3][3] = 0.f;
		proj[2][3] = -n * f / (n - f);
		proj[3][2] = 1.f;
		projInv[0][0] = aspect / ctg;
		projInv[1][1] = 1.f / ctg;
		projInv[2][2] = 0.f;
		projInv[2][3] = 1.f;
		projInv[3][3] = 1.f / f;
		projInv[3][2] = (f - n) / (f * n);
	}

	void Camera::setWorldOffset(const glm::vec3& offset)
	{
		updatedMatrices = false;
		viewInv = glm::row(viewInv, 3, glm::vec4(offset, 1.f));
	}

	void Camera::addWorldOffset(const glm::vec3& offset)
	{
		updatedMatrices = false;
		viewInv = glm::row(viewInv, 3, glm::vec4(offset + position(), 1.f));
	}

	void Camera::addRelativeOffset(const glm::vec3& offset)
	{
		updateBasis(); // requires rotation to be decoded into 3x3 matrix stored in viewInv

		updatedMatrices = false;
		viewInv = glm::row(viewInv, 3, glm::vec4(offset[0] * right() + offset[1] * top() + offset[2] * forward() + position(), 1.f));
	}

	void Camera::setWorldAngles(const math::Angles& angles)
	{
		updatedBasis = false;
		updatedMatrices = false;

		this->angles = angles;
		rotation = glm::quat({ angles.pitch, -angles.yaw, angles.roll });
	}

	void Camera::addWorldAngles(const math::Angles& angles)
	{
		updatedBasis = false;
		updatedMatrices = false;

		this->angles.pitch += angles.pitch;
		this->angles.yaw += angles.yaw;
		this->angles.roll += angles.roll;
		rotation = glm::quat({ this->angles.pitch, this->angles.yaw, this->angles.roll });
	}

	void Camera::addRelativeAngles(const math::Angles& angles)
	{
		updatedBasis = false;
		updatedMatrices = false;

		rotation *= glm::angleAxis(angles.roll, forward());
		rotation *= glm::angleAxis(angles.pitch, right());
		rotation *= glm::angleAxis(angles.yaw, top());

		normalizeRotation();

		auto pitchYawRoll = glm::eulerAngles(rotation);
		this->angles.roll = pitchYawRoll[2];
		this->angles.pitch = pitchYawRoll[0];
		this->angles.yaw = pitchYawRoll[1];
	}

	void Camera::updateBasis()
	{
		if (updatedBasis) return;
		updatedBasis = true;

		auto rotationMat = glm::mat3(rotation);
		viewInv = glm::row(viewInv, 0, glm::vec4(rotationMat[0], viewInv[3][0]));
		viewInv = glm::row(viewInv, 1, glm::vec4(rotationMat[1], viewInv[3][1]));
		viewInv = glm::row(viewInv, 2, glm::vec4(rotationMat[2], viewInv[3][2]));

	}

	// This function must be called each frame to ensure the matrices are updated after camera movement
	void Camera::updateMatrices()
	{
		if (updatedMatrices) return;
		updatedMatrices = true;

		updateBasis();

		math::invertOrthonormal(viewInv, view);

		viewProj = view * proj;
		viewProjInv = projInv * viewInv;

		glm::mat4 viewCameraCentered = view;
		viewCameraCentered[0][3] = 0.f;
		viewCameraCentered[1][3] = 0.f;
		viewCameraCentered[2][3] = 0.f;
		viewProjCameraCentered = viewCameraCentered * proj;
	}

	void Camera::normalizeRotation(bool force)
	{
		rotationNormalizeCounter--;
		if (rotationNormalizeCounter > 0 && !force)
			return;
		rotationNormalizeCounter = 20;
		rotation = glm::normalize(rotation);
		return;
	}
};