#pragma once

#include "math/Box.h"
#include "glm.hpp"
#include <memory>
#include <vector>

namespace math
{
	struct Ray;
};

namespace engine
{
	class Mesh;
	struct MeshIntersection;

	class TriangleOctree
	{
	public:
		TriangleOctree() = default;
		TriangleOctree(const TriangleOctree&) = delete;
		const static int PREFFERED_TRIANGLE_COUNT;
		const static float MAX_STRETCHING_RATIO;

		void clear() { m_mesh = nullptr; }
		bool inited() const { return m_mesh != nullptr; }

		void initialize(const Mesh& mesh);

		bool intersect(const math::Ray& ray, MeshIntersection& nearest) const;

	protected:
		const Mesh* m_mesh = nullptr;
		std::vector<uint32_t> m_triangles;

		math::Box m_box;
		math::Box m_initialBox;

		std::unique_ptr<std::array<TriangleOctree, 8>> m_children;

		void initialize(const Mesh& mesh, const math::Box& parentBox, const glm::vec3& parentCenter, int octetIndex);

		bool addTriangle(uint32_t triangleIndex, const glm::vec3& V1, const glm::vec3& V2, const glm::vec3& V3, const glm::vec3& center);

		bool intersectInternal(const math::Ray& ray, MeshIntersection& nearest) const;
	};
}
