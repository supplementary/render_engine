#pragma once

#include <mutex>
#include "glm.hpp"
#include "../utils/SolidVector.h"
#include "math/MathUtils.h"

namespace engine
{
	class TransformSystem
	{
	public:
		struct Transform
		{
			glm::mat4 transformMat;
			SolidVector<Transform>::ID parentTransformID;
			void addOffset(const glm::vec3& offset)
			{
				transformMat[3][0] += offset.x;
				transformMat[3][1] += offset.y;
				transformMat[3][2] += offset.z;
			}
		};
		TransformSystem(TransformSystem& other) = delete;
		void operator=(const TransformSystem&) = delete;
		static TransformSystem* getInstance();

		void init() {}
		void deinit() { transforms.clear(); }

		Transform& transform(SolidVector<Transform>::ID id);
		glm::mat4 transformMat(SolidVector<Transform>::ID id) const;
		SolidVector<Transform>::ID insert(const Transform& transform) { return transforms.insert(transform); }
		SolidVector<Transform>::ID insert(const glm::vec3& position, const glm::vec3& scale, const math::Angles& angles, SolidVector<Transform>::ID parentTransformID = -1);

	protected:
		SolidVector<Transform> transforms;

	private:
		TransformSystem() {};
		~TransformSystem() {};

		inline static TransformSystem* instance{ nullptr };
		inline static std::mutex mutex{};
	};
}
