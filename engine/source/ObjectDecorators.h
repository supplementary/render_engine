#pragma once

#include "glm.hpp"
#include <memory>
#include "TransformSystem.h"

class IObjectMover
{
public:
	IObjectMover(std::shared_ptr<IObjectMover> mover) : mover(mover) {};
	virtual void move(const glm::vec3& offset) = 0;

protected:
	std::shared_ptr<IObjectMover> mover;
};

class TransformMover : public IObjectMover
{
public:
	TransformMover(int transformID, std::shared_ptr<IObjectMover> mover = nullptr)
		: IObjectMover(mover)
		, transformID(transformID)
	{}
	virtual void move(const glm::vec3& offset) override
	{
		if (mover)
			mover->move(offset);
		auto transformSystem = engine::TransformSystem::getInstance();
		auto& transform = transformSystem->transform(transformID);
		auto parentMatInv = glm::inverse(transformSystem->transformMat(transform.parentTransformID));
		transform.addOffset(parentMatInv*glm::vec4(offset, 0.f));
	}
protected:
	int transformID;
};

template <class T> class ObjectMover : public IObjectMover
{
public:
	ObjectMover(T& object, std::shared_ptr<IObjectMover> mover = nullptr)
		: IObjectMover(mover)
		, object(object)
	{}
	virtual void move(const glm::vec3& offset) override
	{
		if (mover)
			mover->move(offset);
		object.position += offset;
	}
protected:
	T& object;
};