#include "FPSTimer.h"

FPSTimer::FPSTimer(int fpsLimit)
	: startTime(timer::now())
	, initTime(startTime)
	, deltaTime(0.f)
{
	if (fpsLimit > 0)
		frameTime = seconds(1.0 / fpsLimit);
	else
		frameTime = seconds(0);
}

bool FPSTimer::frameTimeElapsed()
{
	auto duration = timer::now() - startTime;
	if (duration > frameTime)
	{
		startTime = timer::now();
		deltaTime = seconds(duration).count();
		return true;
	}
	return false;
}

float FPSTimer::getDeltaTime()
{
	return deltaTime;
}

float FPSTimer::getTotalTime()
{
	auto duration = timer::now() - initTime;
	return seconds(duration).count();
}
