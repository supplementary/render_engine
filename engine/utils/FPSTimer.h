#pragma once

#include <chrono>

using seconds = std::chrono::duration<float>;
using timer = std::chrono::steady_clock;

class FPSTimer
{
public:
	FPSTimer(int fpsLimit = 60);
	bool frameTimeElapsed();
	float getDeltaTime();
	float getTotalTime();

private:
	timer::time_point startTime;
	timer::time_point initTime;
	float deltaTime;
	seconds frameTime;
};