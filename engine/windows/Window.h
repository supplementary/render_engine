#pragma once

#include "../source/InputListener.h"
#include "../include/win.hpp"
#include "../render/D3D.h"
#include <vector>

#include <dxgi.h>

namespace engine::windows
{
	void attachToEventListener(InputListener* listener);

	class Window : public InputListener
	{
	public:
		Window(HINSTANCE appHandle, int windowShowParams);
		Window() = delete;
		~Window();
		void initSwapchain();
		void initBackBuffer();
		inline int getWidth() const { return width; };
		inline int getHeight() const { return height; };
		inline DxResPtr<ID3D11Texture2D> getRenderTarget() { return backbuffer; };
		void flush();
		void onWindowResize(int width, int height) override;

	private:
		HWND hWnd = nullptr;
		DxResPtr<IDXGISwapChain1> swapchain;
		DxResPtr<ID3D11Texture2D> backbuffer;
		D3D11_TEXTURE2D_DESC backbufferDesc;
		int width, height;
		HDC hdc;
	};
}