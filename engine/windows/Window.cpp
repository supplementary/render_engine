#include "Window.h"

#include "windowsx.h"
#include "assert.hpp"
#include <unordered_map>
#include "../render/Renderer.h"

namespace engine::windows
{
    LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);

    static std::unordered_map<HWND, InputListener*> s_windowListeners;
    static InputListener* s_eventListener;

    Window::Window(HINSTANCE appHandle, int windowShowParams)
        : width(0)
        , height(0)
    {
        WNDCLASSEX wc;
        ZeroMemory(&wc, sizeof(WNDCLASSEX));

        wc.cbSize = sizeof(WNDCLASSEX);
        wc.style = CS_HREDRAW | CS_VREDRAW;
        wc.lpfnWndProc = WndProc;
        wc.hInstance = appHandle;
        wc.hCursor = LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
        wc.lpszClassName = L"WindowClass";

        RegisterClassEx(&wc);

        hWnd = CreateWindowEx(NULL,
            L"WindowClass",    // name of the window class
            L"Homework",   // title of the window
            WS_OVERLAPPEDWINDOW,    // window style
            300,    // x-position of the window
            300,    // y-position of the window
            500,    // width of the window
            400,    // height of the window
            NULL,    // we have no parent window, NULL
            NULL,    // we aren't using menus, NULL
            appHandle,    // application handle
            NULL);    // used with multiple windows, NULL

        s_windowListeners[hWnd] = this;
        hdc = GetDC(hWnd);
        initSwapchain();
        ShowWindow(hWnd, windowShowParams);
    }

    Window::~Window()
    {
        swapchain.release();
        backbuffer.release();
        ReleaseDC(hWnd, hdc);
        s_windowListeners.erase(hWnd);
        DestroyWindow(hWnd);
    }

    void Window::initSwapchain()
    {
        DXGI_SWAP_CHAIN_DESC1 desc;

        // clear out the struct for use
        memset(&desc, 0, sizeof(DXGI_SWAP_CHAIN_DESC1));

        // fill the swap chain description struct
        desc.AlphaMode = DXGI_ALPHA_MODE::DXGI_ALPHA_MODE_UNSPECIFIED;
        desc.BufferCount = 2;
        desc.BufferUsage = DXGI_USAGE_BACK_BUFFER | DXGI_USAGE_RENDER_TARGET_OUTPUT;
        desc.Flags = 0;
        desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        desc.SampleDesc.Count = 1;                               // how many multisamples
        desc.SampleDesc.Quality = 0;                             // ???
        desc.Scaling = DXGI_SCALING_NONE;
        desc.Stereo = false;
        desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;

        HRESULT res = engine::d3d::s_factory->CreateSwapChainForHwnd(engine::d3d::s_device, hWnd, &desc, NULL, NULL, swapchain.reset());
        ALWAYS_ASSERT(res >= 0 && "CreateSwapChainForHwnd");
    }

    void Window::initBackBuffer() // may be called after resizing
    {
        if (backbuffer.valid())
        {
            render::Renderer::getInstance()->releaseRTV();
            backbuffer.release();
            swapchain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);
        }

        HRESULT result = swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)backbuffer.reset());
        ALWAYS_ASSERT(result >= 0);

        ID3D11Texture2D* pTextureInterface = 0;
        backbuffer->QueryInterface<ID3D11Texture2D>(&pTextureInterface);
        pTextureInterface->GetDesc(&backbufferDesc);
        pTextureInterface->Release();

        render::Renderer::getInstance()->initDepthAndRTV(backbuffer, width, height);
    }

    void Window::flush()
    {
        swapchain->Present(0, 0);
    }

    void Window::onWindowResize(int width, int height)
    {
        this->width = width;
        this->height = height;
        if (width == 0 || height == 0)
            return;
        initBackBuffer();
        // Set up the viewport.
        D3D11_VIEWPORT vp;
        vp.Width = (float)width;
        vp.Height = (float)height;
        vp.MinDepth = 0.0f;
        vp.MaxDepth = 1.0f;
        vp.TopLeftX = 0;
        vp.TopLeftY = 0;
        engine::d3d::s_devcon->RSSetViewports(1, &vp);
    }

    LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
    {
        switch (message)
        {
        case WM_KEYDOWN:
            s_eventListener->onKbButtonDown((KB_BUTTON)wParam);
            if((KB_BUTTON)wParam == KB_BUTTON::ESC)
                PostQuitMessage(0);
            break;
        case WM_KEYUP:
            s_eventListener->onKbButtonUp((KB_BUTTON)wParam);
            break;
        case WM_MOUSEWHEEL:
            s_eventListener->onMouseWheel(GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA);
            break;
        case WM_LBUTTONDOWN:
            SetCapture(hWnd);
            s_eventListener->onMouseDown(M_BUTTON::LMB);
            break;
        case WM_LBUTTONUP:
            ReleaseCapture();
            s_eventListener->onMouseUp(M_BUTTON::LMB);
            break;
        case WM_RBUTTONDOWN:
            s_eventListener->onMouseDown(M_BUTTON::RMB);
            break;
        case WM_RBUTTONUP:
            s_eventListener->onMouseUp(M_BUTTON::RMB);
            break;
        case WM_MOUSEMOVE:
        {
            RECT rect;
            GetClientRect(hWnd, &rect);
            s_eventListener->onMouseMove((int)wParam, (float)GET_X_LPARAM(lParam) / (rect.right - rect.left), (float)GET_Y_LPARAM(lParam) / (rect.bottom - rect.top));
        }
        break;
        case WM_PAINT:
            ValidateRect(hWnd, NULL);
            break;
        case WM_SIZE:
        {
            RECT rect;
            GetClientRect(hWnd, &rect);
            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;
            s_windowListeners[hWnd]->onWindowResize(width, height);
            s_eventListener->onWindowResize(width, height);
        }
        break;
        case WM_DESTROY:
            PostQuitMessage(0);
            break;
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
        return 0;
    }

    void attachToEventListener(InputListener* listener)
    {
        s_eventListener = listener;
    }
}
