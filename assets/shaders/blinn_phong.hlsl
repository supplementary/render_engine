#include "common.hlsl"

cbuffer MeshData : register(bCB(MESH_TO_MODEL_CBUF))
{
    float4x4 g_meshToModel;
}

struct VertexInput
{
    float3 position : POSITION;
    float2 tc : TEXCOORD0;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    float4 toWorld0 : WORLD0;
    float4 toWorld1 : WORLD1;
    float4 toWorld2 : WORLD2;
    float4 toWorld3 : WORLD3;
    float4 color : COLOR;
};

struct VertexOutput
{
    float4 position : SV_POSITION;
    float3 worldPosition : WORLD_POSITION;
    float2 tc : TEXCOORD0;
    float3 normal : NORMAL;
};

VertexOutput vs_main(VertexInput input)
{
    float4x4 modelToWorld = float4x4(input.toWorld0, input.toWorld1, input.toWorld2, input.toWorld3);
    float4x4 meshToWorld = mul(g_meshToModel, modelToWorld);
    float3 axisX = normalize(meshToWorld[0].xyz);
    float3 axisY = normalize(meshToWorld[1].xyz);
    float3 axisZ = normalize(meshToWorld[2].xyz);
    
    VertexOutput output;
    float4 worldPosition = mul(float4(input.position, 1.0), meshToWorld);
    output.normal = normalize(input.normal.x * axisX + input.normal.y * axisY + input.normal.z * axisZ);
    
    output.position = mul(worldPosition, g_viewProjection);
    output.worldPosition = worldPosition;
    output.tc = input.tc;
    return output;
}

Texture2D<float4> g_colorTexture : register(t0);

float3 pointlightShading(Pointlight light, float3 position, float3 normal, float3 color)
{
    float3 lightDir = normalize(light.position - position);
    float diffAngle = dot(normal, lightDir);
    float diff = max(diffAngle, EPSILON);
    float distance2 = dot(light.position - position, light.position - position);
    float attenuation = 1.f / distance2;
    float3 halfDir = normalize(normalize(- position) + lightDir);
    float spec = dot(normal, halfDir);
    if (diffAngle <= 0)
        spec = 0;
    spec = pow(max(spec, EPSILON), 32.0);
    float diffuse = light.diffusePower * diff;
    float specular = light.specularPower * spec;
    return light.color * color * (diffuse + specular) * attenuation * light.power;
}

float3 sunlightShading(Sunlight light, float3 position, float3 normal, float3 color)
{
    float diffAngle = dot(normal, -light.direction);
    float diff = max(diffAngle, EPSILON);
    float3 halfDir = normalize(normalize(- position) - light.direction);
    float spec = dot(normal, halfDir);
    if (diffAngle <= 0)
        spec = 0;
    spec = pow(max(spec, EPSILON), 32.0);
    float diffuse = light.diffusePower * diff;
    float specular = light.specularPower * spec;
    return light.color * color * (diffuse + specular) * light.power;
}

float3 spotlightShading(Spotlight light, float3 position, float3 normal, float3 color)
{
    float3 lightDir = normalize(light.position - position);
    float diffAngle = dot(normal, lightDir);
    float diff = max(diffAngle, EPSILON);
    float distance2 = dot(light.position - position, light.position - position);
    float distanceAttenuation = 1.f / distance2;
    float cosAngle = dot(lightDir, -light.direction);
    float delta = light.innerCutoff - light.outerCutoff;
    float angularAttenuation = max(min((cosAngle - light.outerCutoff) / delta, 1.f), 0.f);
    float3 halfDir = normalize(normalize(- position) + lightDir);
    float spec = dot(normal, halfDir);
    if (diffAngle <= 0)
        spec = 0;
    spec = pow(max(spec, EPSILON), 32.0);
    float diffuse = light.diffusePower * diff;
    float specular = light.specularPower * spec;
    float3 maskPosition = lightDir - light.direction;
    float2 uv;
    uv.x = dot(maskPosition, light.rightDirection) - 0.5;
    uv.y = dot(maskPosition, light.upDirection) - 0.5;
    float4 mask = g_spotlightMask.Sample(g_linearWrapSampler, uv);
    return light.color * color * mask.xyz * (diffuse + specular) * distanceAttenuation * angularAttenuation * light.power;
}

struct PixelOutput
{
    float4 outputColor : SV_Target0;
};

PixelOutput ps_main(VertexOutput input)
{
    float4 textureColor = g_colorTexture.Sample(g_linearWrapSampler, input.tc.xy);
    float3 color = textureColor.xyz * g_ambientColor;
    for (int i = 0; i < g_numSunlights; i++)
    {
        color += sunlightShading(g_sunLights[i], input.worldPosition, input.normal, textureColor.xyz);
    }
    for (i = 0; i < g_numPointlights; i++)
    {
        color += pointlightShading(g_pointLights[i], input.worldPosition, input.normal, textureColor.xyz);
    }
    for (i = 0; i < g_numSpotlights; i++)
    {
        color += spotlightShading(g_spotLights[i], input.worldPosition, input.normal, textureColor.xyz);
    }
    PixelOutput output;
    output.outputColor = float4(color, 1.0);
    return output;
}