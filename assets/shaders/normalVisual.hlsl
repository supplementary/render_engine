#include "common.hlsl"

cbuffer MeshData : register(bCB(MESH_TO_MODEL_CBUF))
{
    float4x4 g_meshToModel;
}

struct VertexInput
{
    float3 pos : POSITION;
    float2 tc : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    float4 toWorld0 : WORLD0;
    float4 toWorld1 : WORLD1;
    float4 toWorld2 : WORLD2;
    float4 toWorld3 : WORLD3;
    float4 color : COLOR;
};

struct VertexOutput
{
    float4 position : SV_POSITION;
    float3 color : COLOR;
    float3 normal : NORMAL;
};

VertexOutput vs_main(VertexInput vertexInput)
{
    float4x4 modelToWorld = float4x4(vertexInput.toWorld0, vertexInput.toWorld1, vertexInput.toWorld2, vertexInput.toWorld3);
    float4x4 meshToWorld = mul(g_meshToModel, modelToWorld);
    float3 axisX = normalize(meshToWorld[0].xyz);
    float3 axisY = normalize(meshToWorld[1].xyz);
    float3 axisZ = normalize(meshToWorld[2].xyz);
    
    VertexOutput output;
    output.position = mul(mul(float4(vertexInput.pos, 1.0), meshToWorld), g_viewProjection);
    output.color = vertexInput.color;
    output.normal = normalize(vertexInput.normal.x * axisX + vertexInput.normal.y * axisY + vertexInput.normal.z * axisZ);
    return output;
}

struct PixelOutput
{
    float4 color : SV_Target0;
};

PixelOutput ps_main(VertexOutput pixelInput)
{
    PixelOutput output;
    output.color = float4(pixelInput.normal / 2 + 0.5, 1.0);
    return output;
}