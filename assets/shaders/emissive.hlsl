#include "common.hlsl"

cbuffer MeshData : register(bCB(MESH_TO_MODEL_CBUF))
{
    float4x4 g_meshToModel;
}

struct VertexInput
{
    float3 position : POSITION;
    float2 tc : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    float4 toWorld0 : WORLD0;
    float4 toWorld1 : WORLD1;
    float4 toWorld2 : WORLD2;
    float4 toWorld3 : WORLD3;
    float4 color : COLOR;
};

struct VertexOutput
{
    float4 position : SV_POSITION;
    float3 worldPosition : WORLD_POSITION;
    nointerpolation float4 emission : COLOR;
    float3 normal : NORMAL;
};

VertexOutput vs_main(VertexInput input)
{
    float4x4 modelToWorld = float4x4(input.toWorld0, input.toWorld1, input.toWorld2, input.toWorld3);
    float4x4 meshToWorld = mul(g_meshToModel, modelToWorld);
    float3 axisX = normalize(meshToWorld[0].xyz);
    float3 axisY = normalize(meshToWorld[1].xyz);
    float3 axisZ = normalize(meshToWorld[2].xyz);
    
    VertexOutput output;
    float4 worldPosition = mul(float4(input.position, 1.0), meshToWorld);
    output.normal = normalize(input.normal.x * axisX + input.normal.y * axisY + input.normal.z * axisZ);
    
    output.position = mul(worldPosition, g_viewProjection);
    output.worldPosition = worldPosition;
    output.emission = input.color;
    return output;
}

struct PixelOutput
{
    float4 outputColor : SV_Target0;
};

PixelOutput ps_main(VertexOutput input)
{
    float3 cameraDir = normalize(- input.worldPosition);

    float3 normedEmission = input.emission / max(input.emission.x,
	max(input.emission.y, max(input.emission.z, 1.0)));

    float NoV = dot(cameraDir, input.normal);
    PixelOutput output;
    output.outputColor = float4(lerp(normedEmission * 0.33, input.emission, pow(max(0.0, NoV), 8.0)), 1.0);
    return output;
}