#ifndef __COMMON_HLSL__
#define __COMMON_HLSL__

#include "registers.hlsl"

#define CONCAT(a, b) a##b
#define bCB(index) CONCAT(b, index)

#define EPSILON 0.000001

cbuffer FrameData : register(bCB(PER_FRAME_CBUF))
{
    float4 g_mainResolution;
    float4 g_mousePosition;
    float g_time;
    float g_EV100;
};

cbuffer ViewData : register(bCB(PER_VIEW_CBUF))
{
    float4x4 g_viewProjection;
    float3 g_frustumOrigin;
    float3 g_frustumU;
    float3 g_frustumV;
    float3 g_cameraPosition;
};

struct Sunlight
{
    float3 color;
    float power;
    float3 direction;
    float diffusePower;
    float specularPower;
    float3 pad;
};
struct Pointlight
{
    float3 color;
    float power;
    float3 position;
    float diffusePower;
    float specularPower;
    float3 pad;
};
struct Spotlight
{
    float3 color;
    float power;
    float3 position;
    float diffusePower;
    float3 direction;
    float specularPower;
    float3 upDirection;
    float innerCutoff;
    float3 rightDirection;
    float outerCutoff;
};

cbuffer LightsData : register(bCB(LIGHTS_CBUF))
{
    Sunlight g_sunLights[MAX_SUNLIGHTS];
    Pointlight g_pointLights[MAX_POINTLIGHTS];
    Spotlight g_spotLights[MAX_SPOTLIGHTS];
    uint g_numSunlights;
    float3 g_ambientColor;
    uint g_numPointlights;
    uint g_numSpotlights;
}

Texture2D<float4> g_spotlightMask : register(t1);

SamplerState g_pointWrapSampler : register(s0);
SamplerState g_linearWrapSampler : register(s1);
SamplerState g_anisotropicWrapSampler : register(s2);

#endif // __COMMON_HLSL__
