#include "common.hlsl"

struct VertexInput
{
    uint vertexID : SV_VERTEXID;
};

struct VertexOutput
{
    float4 position : SV_POSITION;
    float3 tc : TEXCOORD0;
};

VertexOutput vs_main(VertexInput input)
{
    VertexOutput output;
    if (input.vertexID == 0)
    {
        output.position = float4(-1.0, -1.0, 0.0, 1.0);
        output.tc = g_frustumOrigin;
    }
    else if (input.vertexID == 1)
    {
        output.position = float4(-1.0, 3.0, 0.0, 1.0);
        output.tc = 2 * g_frustumV - g_frustumOrigin;
    }
    else if (input.vertexID == 2)
    {
        output.position = float4(3.0, -1.0, 0.0, 1.0);
        output.tc = 2 * g_frustumU - g_frustumOrigin;
    }
    
    return output;
}

TextureCube<float4> g_colorTexture : register(t0);

struct PixelOutput
{
    float4 outputColor : SV_Target0;
};

PixelOutput ps_main(VertexOutput input)
{
    PixelOutput output;
    output.outputColor = g_colorTexture.Sample(g_linearWrapSampler, input.tc);
    return output;
}