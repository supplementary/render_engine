#include "common.hlsl"

cbuffer MeshData : register(bCB(MESH_TO_MODEL_CBUF))
{
    float4x4 g_meshToModel;
}

// BEGIN ShaderToy https://www.shadertoy.com/view/WttcRB
float hash4d(in float4 p) {
	p = frac(p * 0.1031);
    p += dot(p, p.zwyx + 31.32);
    return frac((p.x + p.y) * p.z - p.x * p.w);
}

float noise4d(in float4 p) {
    float4 cell = floor(p);
    float4 local = frac(p);
    local *= local * (3.0 - 2.0 * local);

    float ldbq = hash4d(cell);
    float rdbq = hash4d(cell + float4(1.0, 0.0, 0.0, 0.0));
    float ldfq = hash4d(cell + float4(0.0, 0.0, 1.0, 0.0));
    float rdfq = hash4d(cell + float4(1.0, 0.0, 1.0, 0.0));
    float lubq = hash4d(cell + float4(0.0, 1.0, 0.0, 0.0));
    float rubq = hash4d(cell + float4(1.0, 1.0, 0.0, 0.0));
    float lufq = hash4d(cell + float4(0.0, 1.0, 1.0, 0.0));
    float rufq = hash4d(cell + float4(1.0, 1.0, 1.0, 0.0));
    float ldbw = hash4d(cell + float4(0.0, 0.0, 0.0, 1.0));
    float rdbw = hash4d(cell + float4(1.0, 0.0, 0.0, 1.0));
    float ldfw = hash4d(cell + float4(0.0, 0.0, 1.0, 1.0));
    float rdfw = hash4d(cell + float4(1.0, 0.0, 1.0, 1.0));
    float lubw = hash4d(cell + float4(0.0, 1.0, 0.0, 1.0));
    float rubw = hash4d(cell + float4(1.0, 1.0, 0.0, 1.0));
    float lufw = hash4d(cell + float4(0.0, 1.0, 1.0, 1.0));
    float rufw = hash4d(cell + 1.0);

    return lerp(lerp(lerp(lerp(ldbq, rdbq, local.x),
                       lerp(lubq, rubq, local.x),
                       local.y),

                   lerp(lerp(ldfq, rdfq, local.x),
                       lerp(lufq, rufq, local.x),
                       local.y),

                   local.z),

               lerp(lerp(lerp(ldbw, rdbw, local.x),
                       lerp(lubw, rubw, local.x),
                       local.y),

                   lerp(lerp(ldfw, rdfw, local.x),
                       lerp(lufw, rufw, local.x),
                       local.y),

                   local.z),

               local.w);
}

float noise4d(in float4 p, uniform in uint octaves)
{
    float nscale = 1.0;
    float tscale = 0.0;
    float value = 0.0;

    for (uint octave=0.0; octave < octaves; octave++) {
        value += noise4d(p) * nscale;
        tscale += nscale;
        nscale *= 0.5;
        p *= 2.0;
    }

    return value / tscale;
}
// END ShaderToy https://www.shadertoy.com/view/WttcRB

float distanceIntensity(float value, float target, float fade)
{
    return min(1.0, abs(value - target) / fade);
}

float periodIntensity(float value, float period, float fade)
{
    float target = round(value / period) * period;
    return distanceIntensity(value, target, fade);
}

float wave(float3 pos, float waveInterval, float waveYSpeed, float waveThickness, uniform bool distort)
{
    if (distort)
    {
        const float WAVE_XZ_SPEED = 3.0;
        const float WAVE_DISTORTION_SIZE = 0.035;
        const float WAVE_OSCILLATING_TIME = 4;

        float distortionSign = abs(frac(g_time / WAVE_OSCILLATING_TIME) - 0.5) * 4 - 1;
        float2 distortion = sin(pos.xz / WAVE_DISTORTION_SIZE + g_time * WAVE_XZ_SPEED) * WAVE_DISTORTION_SIZE * distortionSign;
        pos.y -= (distortion.x + distortion.y);
    }

    pos.y -= g_time * waveYSpeed;

    float intensity = 1.0 - periodIntensity(pos.y, waveInterval, waveThickness);
    return intensity;
}

// Note: in HLSL global constant is marked with "static const".
// One "const" is not enough, because it will be considered to be a uniform from a constant buffer.
// In HLSL const means that the value can not be changed by the shader.
// Adding "static" to global variable means that it is not visible to application, so doesn't belong to a constant buffer.
// A local constant inside a function can be marked just with "const".
// "static" for locals preserves value during current shader thread execution.

static const float BLUE_WAVE_INTERVAL = 0.8;
static const float BLUE_WAVE_SPEED = 0.25;
static const float BLUE_WAVE_THICKNESS = 0.05;

static const float RED_WAVE_INTERVAL = 10;
static const float RED_WAVE_SPEED = 2;
static const float RED_WAVE_THICKNESS = 0.2;

struct VertexInput
{
    float3 position : POSITION;
    float2 tc : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    float4 toWorld0 : WORLD0;
    float4 toWorld1 : WORLD1;
    float4 toWorld2 : WORLD2;
    float4 toWorld3 : WORLD3;
    float4 color : COLOR;
};

struct VertexOutput
{
    float4 position : SV_POSITION;
    float3 worldPosition : WORLD_POSITION;
    float3 wavePosition : WAVE_POSITION;
    nointerpolation float4 color : COLOR;
    float3 normal : NORMAL;
};

VertexOutput vs_main(VertexInput input)
{
    float4x4 modelToWorld = float4x4(input.toWorld0, input.toWorld1, input.toWorld2, input.toWorld3);
    float4x4 meshToWorld = mul(g_meshToModel, modelToWorld);
    float3 axisX = normalize(meshToWorld[0].xyz);
    float3 axisY = normalize(meshToWorld[1].xyz);
    float3 axisZ = normalize(meshToWorld[2].xyz);
    
    VertexOutput output;
    float4 worldPosition = mul(float4(input.position, 1.0), meshToWorld);
    output.normal = normalize(input.normal.x * axisX + input.normal.y * axisY + input.normal.z * axisZ);
    output.wavePosition = mul(input.position, float3x3(g_meshToModel[0].xyz, g_meshToModel[1].xyz, g_meshToModel[2].xyz));
    
    float3 offset = 0.0;
    offset += output.normal * 0.025 * wave(output.wavePosition, BLUE_WAVE_INTERVAL, BLUE_WAVE_SPEED, BLUE_WAVE_THICKNESS, true);
    offset += output.normal * 0.05 * wave(output.wavePosition, RED_WAVE_INTERVAL, RED_WAVE_SPEED, RED_WAVE_THICKNESS, false);
    
    output.position = mul(worldPosition + float4(offset, 0.0), g_viewProjection);
    output.worldPosition = worldPosition;
    output.color = input.color;
    return output;
}

struct PixelOutput
{
    float4 outputColor : SV_Target0;
};

PixelOutput ps_main(VertexOutput input)
{
    float blueWave = wave(input.wavePosition, BLUE_WAVE_INTERVAL, BLUE_WAVE_SPEED, BLUE_WAVE_THICKNESS, true);
    float redWave = wave(input.wavePosition, RED_WAVE_INTERVAL, RED_WAVE_SPEED, RED_WAVE_THICKNESS, false);

    float3 toCamera = normalize(- input.worldPosition);
    float contourGlow = pow(1.0 - abs(dot(input.normal, toCamera)), 2);
    
    // when contourInterference is 0, ripple effect contourWave is added to contourGlow, otherwise contourWave is 1
    float contourWave = wave(input.wavePosition, 0.1, 0.1, 0.05, false);
    float contourInterference = periodIntensity(g_time, 4, 1);
    contourWave = lerp(contourWave, 1.0, contourInterference);
    // when contourWave is 0.0, contourGlow becomes darker, otherwise contourGlow color is plain, without ripple
    contourGlow = lerp(contourGlow / 10, contourGlow, contourWave);

    float3 color = input.color * min(1, contourGlow + blueWave * 0.5);
    float colorNoise = sqrt(noise4d(float4(input.wavePosition, frac(g_time)) * 100, 1));
    color *= lerp(colorNoise, 1.0, contourInterference);
    
    color = lerp(color, float3(1.0, 0.0, 0.0), redWave * 0.25);
    PixelOutput output;
    output.outputColor = float4(color, 1.0);
    return output;
}