#pragma once

#include "source/InputListener.h"
#include <vector>
#include "source/Mesh.h"
#include "render/MeshSystem.h"
#include "source/Camera.h"
#include "windows/Window.h"
#include "glm.hpp"
#include <memory>

class Application : InputListener
{
public:
	Application();
	void init();
	void processInputs(float deltaTime, float totalTime);
	math::Camera& getCamera();
	void render(engine::windows::Window& window);
	void onKbButtonDown(KB_BUTTON b) override;
	void onKbButtonUp(KB_BUTTON b) override;
	void onMouseDown(M_BUTTON m) override;
	void onMouseUp(M_BUTTON m) override;
	void onMouseWheel(int delta) override;
	void onMouseMove(int vKey, float posX, float posY) override;
	void onQuit() override;
	void onWindowResize(int width, int height) override;
	bool hasQuit();

private:
	math::Camera camera;
	float cameraSpeed;
	float EV100;
	int flashlightTransformID;
	bool holdingFlashlight;
	float flashlightToggleDuration;
	bool kbState[256];
	float mousePosX, mousePosY, mouseDeltaX, mouseDeltaY;
	bool exitState;
	
	render::IntersectionQuery clickQuery;
};
