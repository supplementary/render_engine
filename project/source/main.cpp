#include <windows.h>
#include <windowsx.h>
#include <thread>
#include "windows/Window.h"
#include <iostream>
#include "utils/FPSTimer.h"
#include "application.h"
#include "source/Engine.h"

void initConsole()
{
    AllocConsole();
    FILE* dummy;
    auto s = freopen_s(&dummy, "CONOUT$", "w", stdout); // stdout will print to the newly created console
}

int WINAPI WinMain(_In_ HINSTANCE appHandle, _In_opt_ HINSTANCE, _In_ LPSTR cmdLine, _In_ int windowShowParams)
{
    initConsole();
    engine::Engine::init();
    Application application;
    engine::windows::attachToEventListener((InputListener*)&application);
    application.init();
    engine::windows::Window window(appHandle, windowShowParams);
    FPSTimer fpsTimer(60);
    MSG msg;
    while (true)
    {
        // wait for the next message in the queue, store the result in 'msg'
        while (PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
            {
                application.onQuit();
                break;
            }
            TranslateMessage(&msg);
            DispatchMessageW(&msg);
        }

        if (application.hasQuit())
            break;

        if (fpsTimer.frameTimeElapsed()) // enter the scope 60 times per second
        {
            application.processInputs(fpsTimer.getDeltaTime(), fpsTimer.getTotalTime());
            application.render(window);
            window.flush();
        }

        std::this_thread::yield();
    }

    engine::Engine::deinit();

    return (int) msg.wParam;
}


