#include "Application.h"

#include "glm.hpp"
#include "source/Engine.h"
#include "render/MeshSystem.h"
#include "render/Renderer.h"
#include "source/ModelManager.h"
#include "source/TextureManager.h"
#include "source/TransformSystem.h"
#include "render/LightSystem.h"
#include <iostream>
#include <memory>

Application::Application()
	: cameraSpeed(4.f)
	, EV100(0.f)
	, flashlightTransformID(-1)
	, holdingFlashlight(true)
	, flashlightToggleDuration(0.f)
	, mousePosX(0)
	, mousePosY(0)
	, mouseDeltaX(0)
	, mouseDeltaY(0)
	, exitState(false)
{
	memset(kbState, 0, sizeof(kbState));
	clickQuery.nearest.reset();
	camera.setPerspective(glm::pi<float>() * 0.3f, 1.f, 1.f, 100.f);
}

void Application::init()
{
	render::MeshSystem* meshSystem = render::MeshSystem::getInstance();
	engine::ModelManager* modelManager = engine::ModelManager::getInstance();
	engine::TextureManager* textureManager = engine::TextureManager::getInstance();
	auto samuraiModel = modelManager->model("..\\assets\\models\\Samurai\\Samurai.fbx");
	auto knightModel = modelManager->model("..\\assets\\models\\Knight\\Knight.fbx");
	auto eastTowerModel = modelManager->model("..\\assets\\models\\EastTower\\EastTower.fbx");
	auto knightHorseModel = modelManager->model("..\\assets\\models\\KnightHorse\\KnightHorse.fbx");
	auto cubeModel = modelManager->model("..\\assets\\models\\cube.obj");
	auto sphereModel = modelManager->model("UNIT_SPHERE");
	auto flashlightModel = modelManager->model("..\\assets\\models\\flashlight\\Flashlight.FBX");

	auto flashlightTex1 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\flashlight\\Flashlight.dds") });
	auto flashlightTex2 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\flashlight\\Flashlight_Cookie.dds") });
	std::vector<render::TextureMaterial> flashlightTextures({ flashlightTex1, flashlightTex2 });

	auto samuraiTex1 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Samurai\\dds\\Sword_BaseColor.dds") });
	auto samuraiTex2 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Samurai\\dds\\Head_BaseColor.dds") });
	auto samuraiTex3 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Samurai\\dds\\Eyes_BaseColor.dds") });
	auto samuraiTex4 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Samurai\\dds\\Helmet_BaseColor.dds") });
	auto samuraiTex5 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Samurai\\dds\\Decor_BaseColor.dds") });
	auto samuraiTex6 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Samurai\\dds\\Pants_BaseColor.dds") });
	auto samuraiTex7 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Samurai\\dds\\Hands_BaseColor.dds") });
	auto samuraiTex8 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Samurai\\dds\\Torso_BaseColor.dds") });
	std::vector<render::TextureMaterial> samuraiMaterial({ samuraiTex1, samuraiTex2, samuraiTex3, samuraiTex4, samuraiTex5,
		samuraiTex6, samuraiTex7, samuraiTex8 });

	auto knightTex1 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Knight\\dds\\Fur_BaseColor.dds") });
	auto knightTex2 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Knight\\dds\\Pants_BaseColor.dds") });
	auto knightTex3 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Knight\\dds\\Torso_BaseColor.dds") });
	auto knightTex4 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Knight\\dds\\Head_BaseColor.dds") });
	auto knightTex5 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Knight\\dds\\Eyes_BaseColor.dds") });
	auto knightTex6 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Knight\\dds\\Helmet_BaseColor.dds") });
	auto knightTex7 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Knight\\dds\\Skirt_BaseColor.dds") });
	auto knightTex8 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Knight\\dds\\Cloak_BaseColor.dds") });
	auto knightTex9 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\Knight\\dds\\Gloves_BaseColor.dds") });
	std::vector<render::TextureMaterial> knightMaterial({ knightTex1, knightTex2, knightTex3, knightTex4, knightTex5, knightTex6,
		knightTex7, knightTex8, knightTex9 });

	auto knightHorseTex1 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\KnightHorse\\dds\\Armor_BaseColor.dds") });
	auto knightHorseTex2 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\KnightHorse\\dds\\Horse_BaseColor.dds") });
	auto knightHorseTex3 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\KnightHorse\\dds\\Tail_BaseColor.dds") });
	std::vector<render::TextureMaterial> knightHorseMaterial({ knightHorseTex1, knightHorseTex2, knightHorseTex3 });

	auto eastTowerTex1 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\EastTower\\dds\\CityWalls_BaseColor.dds") });
	auto eastTowerTex2 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\EastTower\\dds\\PalaceWall_BaseColor.dds") });
	auto eastTowerTex3 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\EastTower\\dds\\Trims_BaseColor.dds") });
	auto eastTowerTex4 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\EastTower\\dds\\Statue_BaseColor.dds") });
	auto eastTowerTex5 = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\EastTower\\dds\\StoneWork_BaseColor.dds") });
	std::vector<render::TextureMaterial> eastTowerMaterial({ eastTowerTex1,eastTowerTex2,eastTowerTex3,eastTowerTex4,eastTowerTex5 });

	auto crateTex = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\crate.dds") });
	std::vector<render::TextureMaterial> cube1Material({ crateTex });

	auto grassTex = render::TextureMaterial({ textureManager->texture("..\\assets\\models\\grass.dds") });
	std::vector<render::TextureMaterial> cube2Material({ grassTex });

	engine::TransformSystem* transformSystem = engine::TransformSystem::getInstance();

	meshSystem->addOpaqueInstance(knightModel, knightMaterial, transformSystem->insert({ 0, 0, 0 }, { 1, 1, 1 }, { 0, 0, 0 }));
	meshSystem->addOpaqueInstance(samuraiModel, samuraiMaterial, transformSystem->insert({ 2, 2, 0 }, { 1, 1, 1 }, { 0, 0, 0 }));
	meshSystem->addOpaqueInstance(samuraiModel, samuraiMaterial, transformSystem->insert({ 4, 2, 0 }, { 2, 2, 2 }, { 1, 0.5, 0 }));
	meshSystem->addOpaqueInstance(eastTowerModel, eastTowerMaterial, transformSystem->insert({ 0, 1, 6 }, { 1, 1, 1 }, { 0, 0, 0 }));
	meshSystem->addOpaqueInstance(knightHorseModel, knightHorseMaterial, transformSystem->insert({ -1, -2, 1 }, { 1, 1, 1 }, { 0, 0, 0 }));
	meshSystem->addOpaqueInstance(cubeModel, cube1Material, transformSystem->insert({ -4, 0, 0 }, { 1, 1, 1 }, { 0, 0, 0 }));
	meshSystem->addOpaqueInstance(cubeModel, cube2Material, transformSystem->insert({ -4, 0, 3 }, { 1, 1, 1 }, { 0.5, 0, 0 }));

	render::LightSystem* lightSystem = render::LightSystem::getInstance();
	int transformID;
	render::LightSystem::Pointlight point1;
	point1.color = { 0.8f, 0.8f, 0.f };
	point1.diffusePower = 1.f;
	point1.specularPower = 1.f;
	point1.power = 32.f;
	transformID = transformSystem->insert({ 6,6,0 }, { 0.1,0.1,0.1 }, { 0,0,0 });
	lightSystem->addPointlight(point1, transformID);
	meshSystem->addEmissiveInstance(sphereModel, point1.color * 100.f, transformID);

	render::LightSystem::Pointlight point2;
	point2.color = { 0.f, 0.8f, 0.f };
	point2.diffusePower = 1.f;
	point2.specularPower = 1.f;
	point2.power = 32.f;
	transformID = transformSystem->insert({ 0,-5,0 }, { 0.1,0.1,0.1 }, { 0,0,0 });
	lightSystem->addPointlight(point2, transformID);
	meshSystem->addEmissiveInstance(sphereModel, point2.color * 100.f, transformID);

	render::LightSystem::Pointlight point3;
	point3.color = { 0.1f, 0.1f, 0.9f };
	point3.diffusePower = 1.f;
	point3.specularPower = 1.f;
	point3.power = 32.f;
	transformID = transformSystem->insert({ -2,1.1,-4 }, { 0.1,0.1,0.1 }, { 0,0,0 });
	lightSystem->addPointlight(point3, transformID);
	meshSystem->addEmissiveInstance(sphereModel, point3.color * 100.f, transformID);

	render::LightSystem::Pointlight point4;
	point4.color = { 0.1f, 0.3f, 0.7f };
	point4.diffusePower = 1.f;
	point4.specularPower = 1.f;
	point4.power = 32.f;
	transformID = transformSystem->insert({ 2,4,5 }, { 0.1,0.1,0.1 }, { 0,0,0 });
	lightSystem->addPointlight(point4, transformID);
	meshSystem->addEmissiveInstance(sphereModel, point4.color * 100.f, transformID);

	render::LightSystem::Sunlight sun;
	sun.color = { 1.0f, 1.0f, 1.0f };
	sun.diffusePower = 1.f;
	sun.specularPower = 1.f;
	sun.power = 0.05f;
	transformID = transformSystem->insert({ 0,0,0 }, { 1,1,1 }, { 0,2,-0.6 });
	lightSystem->addSunlight(sun, transformID);

	render::LightSystem::Spotlight spotlight;
	spotlight.color = { 0.95f, 0.95f, 0.5f };
	spotlight.innerCutoff = 0.95f;
	spotlight.outerCutoff = 0.88f;
	spotlight.diffusePower = 1.f;
	spotlight.specularPower = 1.f;
	spotlight.power = 15.f;
	flashlightTransformID = transformSystem->insert({ 0,0,0 }, { 1,1,1 }, { 0,0,0 });
	transformID = transformSystem->insert({ 0,-1.2,2.7 }, { 0.05,0.05,0.05 }, { 0,1.6,0 }, flashlightTransformID);
	lightSystem->addSpotlight(spotlight, transformSystem->insert({ 0,5,0 }, { 1,1,1 }, { 0,-1.6,0 }, transformID));
	lightSystem->setSpotlightMask(flashlightTex2.texture);
	meshSystem->addOpaqueInstance(flashlightModel, flashlightTextures, transformID);

	lightSystem->setAmbientColor({ 0,0,0 });

	render::Renderer::getInstance()->setSkyTexture(textureManager->texture("..\\assets\\night_skybox.dds"));

	camera.setWorldOffset({ -10.f, 5.f, -7.f });
	camera.setWorldAngles({ 0.0f, 0.2f, 0.8f });
}

void Application::processInputs(float deltaTime, float totalTime)
{
	float cameraMovementSpeed = kbState[KB_BUTTON::SHIFT] * 4.f + 1.f;
	cameraMovementSpeed *= cameraSpeed * deltaTime;

	glm::vec3 cameraOffset = (
		float(kbState[KB_BUTTON::W] - kbState[KB_BUTTON::S]) * camera.forward()
		+ float(kbState[KB_BUTTON::D] - kbState[KB_BUTTON::A]) * camera.right()
		+ float(kbState[KB_BUTTON::E] - kbState[KB_BUTTON::Q]) * camera.top()) * cameraMovementSpeed;
	camera.addWorldOffset(cameraOffset);

	camera.addWorldAngles({ 0.f, mouseDeltaY * deltaTime * glm::pi<float>() * 2.f, mouseDeltaX * deltaTime * glm::pi<float>() * 2.f });

	EV100 += ((kbState[KB_BUTTON::PLUS] || kbState[KB_BUTTON::NUM_PLUS]) - (kbState[KB_BUTTON::MINUS] || kbState[KB_BUTTON::NUM_MINUS])) * deltaTime;

	if (kbState[KB_BUTTON::F])
	{
		flashlightToggleDuration += deltaTime;
		if (flashlightToggleDuration > 0.0f)
		{
			flashlightToggleDuration = -0.4f;
			holdingFlashlight = !holdingFlashlight;
		}
	}
	else
		flashlightToggleDuration = 0.0f;

	if (holdingFlashlight)
	{
		engine::TransformSystem::getInstance()->transform(flashlightTransformID).transformMat = glm::transpose(camera.viewInvMatrix());
	}

	if (clickQuery.nearest.valid() && clickQuery.mover)
	{
		clickQuery.nearest.pos -= cameraOffset;
		camera.updateMatrices();
		auto rayEnd = camera.viewProjectionInv(glm::vec4(2 * mousePosX - 1.f, - 2 * mousePosY + 1.f, 1.f, 1.f));
		auto rayDir = glm::vec3(rayEnd) - camera.position();
		rayDir = glm::normalize(rayDir);
		rayDir *= clickQuery.nearest.t;
		auto offset = rayDir - clickQuery.nearest.pos;
		clickQuery.nearest.pos += offset;
		clickQuery.mover->move(offset);
	}

	engine::Engine::g_frameCBuffer.time = totalTime;
	engine::Engine::g_frameCBuffer.mousePosition[0] = mousePosX;
	engine::Engine::g_frameCBuffer.mousePosition[1] = mousePosY;
	engine::Engine::g_frameCBuffer.mousePosition[2] = 1.f / mousePosX;
	engine::Engine::g_frameCBuffer.mousePosition[3] = 1.f / mousePosY;
	engine::Engine::g_frameCBuffer.EV100 = EV100;

	return;
};

math::Camera& Application::getCamera()
{
	return camera;
}

void Application::render(engine::windows::Window& window)
{
	camera.updateMatrices();
	auto x = glm::vec4(0.f, 0.f, 0.f, 1.f);
	glm::vec4 viewportOrigin = (camera.projectionInv({ -1.f, -1.f, 0.f, 1.f }) - x) * camera.viewInvMatrix();
	glm::vec4 viewportU = (camera.projectionInv({ 1.f, -1.f, 0.f, 1.f }) - x) * camera.viewInvMatrix();
	glm::vec4 viewportV = (camera.projectionInv({ -1.f, 1.f, 0.f, 1.f }) - x) * camera.viewInvMatrix();
	render::Renderer::getInstance()->updateViewBuffer(camera.viewProjectionCCMatrix(), viewportOrigin, viewportU, viewportV, camera.position());
	render::Renderer::getInstance()->render();
	return;
}

void Application::onKbButtonDown(KB_BUTTON b)
{
	kbState[b] = 1;
}

void Application::onKbButtonUp(KB_BUTTON b)
{
	kbState[b] = 0;
}

void Application::onMouseDown(M_BUTTON m)
{
	if (m & M_BUTTON::RMB)
	{
		camera.updateMatrices();
		math::Ray ray;
		auto rayPosition = camera.viewProjectionInv(glm::vec4(2*mousePosX-1.f, -2*mousePosY+1.f, 1.f, 1.f));
		ray.set({0,0,0}, glm::vec3(rayPosition) - camera.position());
		render::MeshSystem::getInstance()->findIntersection(ray, clickQuery);
	}
}

void Application::onMouseUp(M_BUTTON m)
{
	if (m & M_BUTTON::RMB)
		clickQuery.nearest.reset();
	if (m & M_BUTTON::LMB)
	{
		mouseDeltaX = 0;
		mouseDeltaY = 0;
	}
}

void Application::onMouseWheel(int delta)
{
	cameraSpeed *= powf(1.05f, (float)delta);
}

void Application::onMouseMove(int vKey, float posX, float posY)
{
	if (vKey & M_BUTTON::LMB)
	{
		mouseDeltaX += (posX - mousePosX);
		mouseDeltaY += (posY - mousePosY);
	}
	mousePosX = posX;
	mousePosY = posY;
}

void Application::onQuit()
{
	exitState = true;
}

void Application::onWindowResize(int width, int height)
{
	camera.setPerspective(glm::pi<float>() * 0.3f, (float)width/(float)height, 0.05f, 100.f);
	engine::Engine::g_frameCBuffer.mainResolution[0] = (float)width;
	engine::Engine::g_frameCBuffer.mainResolution[1] = (float)height;
	engine::Engine::g_frameCBuffer.mainResolution[2] = 1.f / width;
	engine::Engine::g_frameCBuffer.mainResolution[3] = 1.f / height;
}

bool Application::hasQuit()
{
	return exitState;
}
